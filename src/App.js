
import React from "react";
import "./App.css";
import Login from "./pages/login";
import SignIn from "./pages/signIn";
import { Routes, Route, useLocation } from "react-router-dom";
import { Container, CssBaseline, useMediaQuery } from "@mui/material";
import MiniDrawer from "./components/sideBar";
import AdminDashboard from "./pages/admin_dashboard/admin_dashboard";
import AdminUser from "./pages/admin_dashboard/admin_user";
import DataGridDemo from "./components/table/dataGrid";
import Forget from "./pages/forget";
import Paramétre from "./components/admin_parametre/Paramétre";
import UserCotisation from "./pages/user_cotisation/userCotisation"
import Cotisation from "./pages/admincotisation/Cotisation"
import PrivateRoute from "./pages/private_route/private_route";
import AdminCotisation from './pages/admin_cotisation/AdminCotisation'
import AdminCotisation1 from "./pages/admin_cotisation/AdminCotisation1";
import Utilisateur from './pages/admin_cotisation/Utilisateur';
import Archiver from './pages/admin_cotisation/Archiver'
import DetailTerminer from './pages/admin_cotisation/DetailTerminer'
import DetailEnCour from "./pages/admin_cotisation/DetailEnCour";
import DetailBloque from "./pages/admin_cotisation/DetailBloque";
import CotisationTerminer from './pages/admin_cotisation/CotisationTerminer'
import ModifiePassword from "./pages/admin_cotisation/ModifiePassword";
import AdminProfil from "./pages/admin_cotisation/AdminProfil";
import MembresBloquer from "./pages/admin_cotisation/MembresBloquer"
import Information from "./pages/admin_cotisation/Information";
import EditProfil from "./pages/admin_cotisation/EditProfil"





const App = () => {

  const locationUrl = useLocation();
  const matches = useMediaQuery("(min-width:980px)");

  const currentUrl = locationUrl.pathname.split("/").join("");
  const defaultRoute = currentUrl ? Number(currentUrl.split("/")[1]) : 1;

  // console.log(defaultRoute)
  const desktop = () => {
    return (
      <div className="App">
        {!defaultRoute &&
          currentUrl !== "login" &&
          currentUrl !== "signup" &&
          currentUrl !== "forget" && <MiniDrawer />}
        <CssBaseline />
        <Container disableGutters maxWidth="xl" sx={{ pb: 0 }}>
          <Routes>
            <Route path="/" element={<Login />} />
            <Route path="forget" element={<Forget />} />
            <Route path="cotisation" element={<UserCotisation />} />
            <Route path="admin" element={<PrivateRoute />}>
              <Route path="/admin/*" element={
                <Routes>
                  {/* //   <Route path="admin" element={<RouterOutlet />}> */}
                  <>
                    <Route path="dashboard" element={<AdminDashboard />} />
                    <Route path="user" element={<AdminUser />} />
                    <Route path="cotisation" element={<Cotisation />} />
                    <Route path="parametre" element={<Paramétre />} />
                    <Route path="adminCotisation" element={<AdminCotisation />} />
                    <Route path="adminCotisation1" element={<AdminCotisation1 />} />
                    <Route path="CotisationTerminer" element={<CotisationTerminer />} />
                    <Route path="utilisateurs" element={<Utilisateur />} />
                    <Route path="detailTerminer" element={<DetailTerminer/>} />
                    <Route path="detailEncour" element={<DetailEnCour />} />
                    <Route path="detailBloque" element={<DetailBloque/>} />
                    <Route path="modifier" element={<ModifiePassword />} />
                    <Route path="archiver" element={<Archiver />} />
                    <Route path="adminProfil" element={<AdminProfil/>}/>
                    <Route path="membresBloquer" element={<MembresBloquer/>} />
                    <Route path="information" element={<Information/>} />
                    <Route path="editProfil" element={<EditProfil/>} />
                  </>
                  {/* //   </Route> */}
                </Routes>
              } />
            </Route>
          </Routes>
        </Container>
      </div>
    );
  };

  return matches ? (
    desktop()
  ) : (
    <div className="App">
      {!defaultRoute &&
        currentUrl !== "login" &&
        currentUrl !== "signup" &&
        currentUrl !== "forget" && <MiniDrawer />}
      <CssBaseline />
      <Container
        disableGutters
        maxWidth="xl"
        sx={{ pb: 0, fontFamily: "Montserrat" }}
      >
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="signup" element={<SignIn />} />
          <Route path="test" element={<DataGridDemo />} />
          <Route path="forget" element={<Forget />} />

          <Route path="admin">
            <Route path="dashboard" element={<AdminDashboard />} />
            <Route path="user" element={<AdminUser />} />
            <Route path="cotisation" element={<Cotisation />} />
          </Route>
        </Routes>
      </Container>
    </div>
  );
};

export default App;
