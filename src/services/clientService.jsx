import axios from "axios";

const client = axios.create({
  baseURL: "https://cotisation-api-app.herokuapp.com" ,
//   baseURL: "http://127.0.0.1:8000/api/auth/login" 
});

export default client;