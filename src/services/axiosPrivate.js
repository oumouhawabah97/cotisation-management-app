import axios from "axios";
const userData = localStorage.getItem("user");

const privateInstance = axios.create({
  baseURL: "https://cotisation-api-app.herokuapp.com",
  headers: { "Authorization": "Bearer "+userData.accessToken },
});


export default privateInstance;