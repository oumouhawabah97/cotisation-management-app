import React from "react"
function tokenHeader() {
  const user = JSON.parse(localStorage.getItem("user"))

  if (user && user.accessToken) {

    return { "Authorization": "Bearer " + user.accessToken }
  }
}

export default function useAuth() {
  const [auth, setAuth] = React.useState(null)
  React.useEffect(() => {
    setAuth(JSON.parse(localStorage.getItem("user")))
  }, [])
  return {
    auth: auth,
    authHeader: tokenHeader,
  };
}
