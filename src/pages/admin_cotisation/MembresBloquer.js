import React from 'react'
import { BiBlock } from 'react-icons/bi'
import client from '../../services/clientService'
import useAuth from '../../hooks/useAuth';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
function createData(name, calories, fat) {
  return { name, calories, fat };
}

const rows = [
  createData('Selena Roy', '01/01/2022',),
  createData(' Emma Watson', '01/01/2022',),
  createData('Jhon Robert', '01/01/2022',),
  createData(' Anne Hathaway', '01/01/2022',),
  createData('Ravi Shankar', '01/01/2022',),
];



export default function MembresBloauer() {

  const [userBloquer, setUserBloquer] = React.useState([]);
  const { authHeader} = useAuth()
  
  
  React.useEffect(() => {
    const getBlockedUsers = async () => {
      const response = await client.get("/users/blocked", { headers: authHeader() })
      const { data } = await response
      setUserBloquer(data)
    }
    getBlockedUsers()
  }, [authHeader])
  return (

    <>
      <div className='row'>
        <div className='col-2'>

        </div>
        <div className='col-9 mt-4'>
          <p className='para mt-5 ' style={{ textAlign: 'start', fontSize: 18, fontFamily: 'Montserrat' }}> Membres Bloqués</p>
          <div class="  shadow  " data-sr-id='7'>
            <h1 className='titl mt-3' style={{ fontSize: '18', fontFamily: 'Montserrat' }}>6 Membres Bloqués</h1>


            <TableContainer component={Paper} style={{ marginTop: 20 }} >
              <Table sx={{ minWidth: 400 }} aria-label="simple table borderless">
                <TableHead style={{ background: '#20DF7F' }}>
                  <TableRow>
                    <TableCell style={{ fontSize: 20, fontFamily: 'Montserrat' }} >Membres</TableCell>
                    <TableCell style={{ fontSize: 20, fontFamily: 'Montserrat' }}>Date</TableCell>
                    <TableCell style={{ fontSize: 20, fontFamily: 'Montserrat' }}>Actions</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {userBloquer.map((row) => (
                    <TableRow
                      key={row.phone}
                      sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                      <TableCell component="th" scope="row" fontFamily='Montserrat' size='26' >
                        {`${row.firstname} ${row.lastname}`}
                      </TableCell>
                      <TableCell >{row.birthday}</TableCell>
                      <TableCell >{row.blocked}
                      <BiBlock  style={{ fontSize: 18, color: '#FF0000', marginLeft: 30 }} /></TableCell>

                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>

            <div className='pigination mt-2'>
              <ul class="pagination d-flex justify-content-center flex-wrap pagination-rounded-flat pagination-success"
                style={{
                  display: 'flex',
                  paddingLeft: 0,
                  listStyle: 'none',
                  borderRadius: 0.25
                }} >

                <li class="page-item"><a class="page-link" href="/" style={{ color: 'black' }}>Previous Page</a></li>
                <li class="page-item" style={{ border: 'none' }}>
                  <a class="page-link" href="/" data-abc="true"
                    style={{
                      color: 'black', border: 'none',
                      borderRadius: 50
                    }}>
                    <i class="fa fa-angle-left"></i></a></li>
                <li class="page-item active" style={{ border: 'none' }}>
                  <a class="page-link" href="/" data-abc="true"
                    style={{
                      color: 'white',
                      border: 'none',
                      background: '#20DF7F',
                      borderRadius: 50
                    }} >1</a></li>

                <li class="page-item" style={{ border: 'none' }}>
                  <a class="page-link" href="/" data-abc="true"
                    style={{
                      color: 'black', border: 'none',
                      borderRadius: 50
                    }}>2</a>
                </li>

                <li class="page-item" style={{ border: 'none' }}>
                  <a class="page-link" href="/" data-abc="true"
                    style={{
                      color: 'black', border: 'none',
                      borderRadius: 50
                    }}>3</a>
                </li>

                <li class="page-item"><a class="page-link" href="/" data-abc="true"
                  style={{
                    color: 'black', border: 'none',
                    borderRadius: 50
                  }}
                ><i class="fa fa-angle-right"></i></a></li>
                <li class="page-item"><a class="page-link" href="/" style={{ color: 'black' }}>Next Page</a></li>
              </ul>
            </div>




          </div>
        </div>
      </div>
    </>

  )
}
