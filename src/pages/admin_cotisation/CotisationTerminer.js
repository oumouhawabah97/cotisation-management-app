import React from 'react'
import { GrView } from 'react-icons/gr'



import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [

  createData('Selena Roy', '01/01/2021','01/01/2022' , '300.000FCFA', 'Terminer'),
  createData(' Emma Watson','01/01/2021' ,'01/01/2022' ,  '300.000FCFA', 'Terminer'),
  createData('Jhon Robert','01/01/2021', '01/01/2022' ,  '300.000FCFA', 'Terminer' ),
  createData(' Anne Hathaway', '01/01/2021', '01/01/2022', '300.000FCFA', 'Terminer' ),
  createData('Ravi Shankar',  '01/01/2021', '01/01/2022' , '300.000FCFA', 'Terminer'),
];

export default function CotisationTerminer() {
  return (
    <div>
           <div className='row'>
      <div className='col-2'>

      </div>
      <div className='col-9'>
        <div class="row">
      
       <div className='  mt-5'>
    
       <TableContainer component={Paper} style={{marginTop:50}} >
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead style={{background:'#20DF7F'}}>
          <TableRow>
            <TableCell align="center"  style={{ fontSize: 18, fontFamily: 'Montserrat' }}>Membres</TableCell>
            <TableCell align="center"  style={{ fontSize: 18, fontFamily: 'Montserrat' }}>Date Debut</TableCell>
            <TableCell align="center"  style={{ fontSize: 18, fontFamily: 'Montserrat' }}>Date Fin</TableCell>
            <TableCell align="center"  style={{ fontSize: 18, fontFamily: 'Montserrat' }}>Montant Total cotisé</TableCell>
            <TableCell align="center"  style={{ fontSize: 18, fontFamily: 'Montserrat' }}>Statut</TableCell>
            <TableCell align="center"  style={{ fontSize: 18, fontFamily: 'Montserrat' }}>Actions </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row" align="center">
                {row.name}
              </TableCell>
              <TableCell align="center">{row.calories}</TableCell>
              <TableCell align="center">{row.fat}</TableCell>
              <TableCell align="center">{row.carbs}</TableCell>
              <TableCell align="center" >{row.protein }</TableCell>
              <TableCell align="center"><GrView /></TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
      


    <div className='pigination mt-2'>
          <ul class="pagination d-flex justify-content-center flex-wrap pagination-rounded-flat pagination-success"
            style={{
              display: 'flex',
              paddingLeft: 0,
              listStyle: 'none',
              borderRadius: 0.25
            }} >

            <li class="page-item"><a class="page-link" href="/" style={{ color: 'black' }}>Previous Page</a></li>
            <li class="page-item" style={{ border: 'none' }}>
              <a class="page-link" href="/" data-abc="true"
                style={{
                  color: 'black', border: 'none',
                  borderRadius: 50
                }}>
                <i class="fa fa-angle-left"></i></a></li>
            <li class="page-item active" style={{ border: 'none' }}>
              <a class="page-link" href="/" data-abc="true"
                style={{
                  color: 'white', 
                  border: 'none',
                  background:'#20DF7F',
                  borderRadius: 50
                }} >1</a></li>

            <li class="page-item" style={{ border: 'none' }}>
              <a class="page-link" href="/" data-abc="true"
                style={{
                  color: 'black', border: 'none',
                  borderRadius: 50
                }}>2</a>
            </li>
          
            <li class="page-item"><a class="page-link" href="/" data-abc="true"
              style={{
                color: 'black', border: 'none',
                borderRadius: 50
              }}
            ><i class="fa fa-angle-right"></i></a></li>
            <li class="page-item"><a class="page-link" href="/" style={{ color: 'black' }}>Next Page</a></li>
          </ul>
        </div>

      </div>
    </div>
    </div>
    </div>
    </div>
  )
}

