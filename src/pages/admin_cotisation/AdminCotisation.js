import React from 'react'
import { GrView } from 'react-icons/gr'
import {BiArchiveIn} from 'react-icons/bi'

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];





// import {IoEyeSharp}  from 'react-icons/io'



export default function AdminCotisation() {
  return (
    <div className='row'>
      <div className='col-2'>

      </div>
      <div className='col-9'>
        <div class="row row-cols-1 row-cols-md-3 g-4 mt-5">
          <div class="col">
            <div class="card flex shadow mt-5" data-sr-id='7' >

              <div class="card-body mt-4 ">
                <p class="card-title" style={{textAlign:'start', fontFamily:'Montserrat',fontWeight:'bold',color:'#093545',fontSize:13}} >Mai</p>
                <p style={{textAlign:'start', fontFamily:'Montserrat',fontWeight:'bold',color:'#093545',fontSize:22}}>225.000FCFA</p>
              </div>
            </div>
          </div>
          <div class="col">
            <div class="card flex shadow mt-5 w-60" data-sr-id='7' >

              <div class="card-body mt-4">
                <p class="card-title" style={{textAlign:'start', fontFamily:'Montserrat',fontWeight:'bold',color:'#093545',fontSize:13}}>Juin</p>
                <p style={{textAlign:'start' ,color:'#093545', fontFamily:'Montserrat',fontWeight:'bold',fontSize:22}}>100.000FCFA</p>
              </div>
            </div>
          </div>
          <div class="col">
            <div class="card flex shadow mt-5" data-sr-id='7'>

              <div class="card-body pt-1 " >
                <p class="card-title" style={{textAlign:'start', fontFamily:'Montserrat',fontWeight:'bold',color:'#093545',fontSize:13}}>Total Caisse</p>
                <p style={{textAlign:'start',color:'#093545', fontFamily:'Montserrat',fontWeight:'bold',fontSize:22}}>225.000FCFAF</p>
                <div class="progress-bar " role="progressbar" style={{ background: '#20DF7F', height: 5 ,width:100}} aria-valuenow="70" aria-valuemin="0" aria-valuemax="70"></div>
                <p className='sel ' style={{textAlign:'end', fontFamily:'Montserrat',fontWeight:'bold',color:'#093545',fontSize:11}} >50% du Seuil</p>

              </div>
            </div>
          </div>


        </div>
        <TableContainer component={Paper} mt={2}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead style={{background:'#20DF7F'}}>
          <TableRow>
            <TableCell>Membres</TableCell>
            <TableCell align="right">Date Debut</TableCell>
            <TableCell align="right">Date Fin</TableCell>
            <TableCell align="right">Montat cotisé</TableCell>
            <TableCell align="right">Statut</TableCell>
            <TableCell align="right">Actions </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow
              key={row.name}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>
              <TableCell align="right">{row.calories}</TableCell>
              <TableCell align="right">{row.fat}</TableCell>
              <TableCell align="right">{row.carbs}</TableCell>
              <TableCell align="right">{row.protein}</TableCell>
              <TableCell align="right"><BiArchiveIn/><GrView /></TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
      

    
       
      


        <div className='pigination mt-2'>
          <ul class="pagination d-flex justify-content-center flex-wrap pagination-rounded-flat pagination-success"
            style={{
              display: 'flex',
              paddingLeft: 0,
              listStyle: 'none',
              borderRadius: 0.25
            }} >

            <li class="page-item"><a class="page-link" href="/" style={{ color: 'black' }}>Previous Page</a></li>
            <li class="page-item" style={{ border: 'none' }}>
              <a class="page-link" href="/" data-abc="true"
                style={{
                  color: 'black', border: 'none',
                  borderRadius: 50
                }}>
                <i class="fa fa-angle-left"></i></a></li>
            <li class="page-item active" style={{ border: 'none' }}>
              <a class="page-link" href="/" data-abc="true"
                style={{
                  color: 'black', border: 'none',
                  borderRadius: 50
                }} >1</a></li>

            <li class="page-item" style={{ border: 'none' }}>
              <a class="page-link" href="/" data-abc="true"
                style={{
                  color: 'black', border: 'none',
                  borderRadius: 50
                }}>2</a>
            </li>
            <li class="page-item" style={{ border: 'none' }}>
              <a class="page-link" href="/" data-abc="true"
                style={{
                  color: 'black', border: 'none',
                  borderRadius: 50
                }}
              >3</a></li>
            <li class="page-item" style={{ border: 'none' }}><a class="page-link" href="/" data-abc="true"
              style={{
                color: 'black', border: 'none',
                borderRadius: 50
              }} >4</a></li>
            <li class="page-item"><a class="page-link" href="/" data-abc="true"
              style={{
                color: 'black', border: 'none',
                borderRadius: 50
              }}
            ><i class="fa fa-angle-right"></i></a></li>
            <li class="page-item"><a class="page-link" href="/" style={{ color: 'black' }}>Next Page</a></li>
          </ul>
        </div>

      </div>
    </div>
  
   



  )
}
