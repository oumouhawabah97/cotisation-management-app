
import React from 'react'
import swal from 'sweetalert'
import images2 from './images2.jpg'

export default function ModifiePassword() {

  const mosAlert = (e) => {
    swal({
    
        title:'Votre mot de passe a été modifié',
        icon:'success',
        button:'OK',
        fontFamily:"Montserrat"
    })
}
    
  return (
    <div>
          <div className='row'>
      <div className='col-2'>

      </div>
      <div className='col-4'>
     
        <div class="row row-cols-1 row-cols-md-7 g-4 mt-5">
          <center>
        <div class="card  flex shadow  mt-5  "  data-sr-id='7'>
  
  <div class="card-body">
  <center>
   
            <img src={images2} class="card-img-top   pl-5" alt='img' style={{ height: 90, width: 92, borderRadius: 600 }}  />
            <h3  style={{fontFamily:'Montserrat',fontSize:22, color:'#093545'}}>Anta Gueye</h3>
              <h5 class="card-text" style={{fontFamily:'Montserrat'}}>admin</h5>
              <p className='title mt-4' style={{fontFamily:'Montserrat',fontSize:28}}>Modifier votre mot de passe</p>
          </center>
    <input className='btn m-3' type='text'  placeholder='Mot de passe actuel' style={{ background: "#093545", height: '51px', borderRadius: '10px',width:'260px',fontFamily: 'Montserrat', color:'#093545', textAlign:'start',padding: 25}} />
    <br/>
    <input className='btn m-3' type='text'  placeholder='Nouveau mot de passe ' style={{ background: "#093545", height: '51px',borderRadius: '10px',width:'260px',fontFamily: 'Montserrat',textAlign:'start' , color:'#093545',padding: 25}}  />
    <br/>
    <input className='btn m-3' type='text'  placeholder='Confirmer mot de passe ' style={{ background: "#093545", height: '51px', borderRadius: '10px',width:'260px',fontFamily: 'Montserrat',textAlign:'start' , color:'#093545',padding: 25}} />
    <br/>
    <button  onClick={() => mosAlert()} class="btn btn-success m-5" style={{color:'#093545', borderRadius: '10px', width:'100'}} >Valider</button>
  </div>
</div>

</center>
</div>
        
     
          </div>
          </div>
          </div>



      


        
  )
}
