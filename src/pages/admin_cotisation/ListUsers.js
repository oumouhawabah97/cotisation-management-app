import React from 'react'
import {Box, List ,ListItem,ListItemText,Divider, ListItemButton} from '@mui/material'
import Pagination from '@mui/material/Pagination';
import Stack from '@mui/material/Stack';

export default function ListUsers() {
  return (
  <Box sx={
   {
    width:'200px', bgcolor:'#fff',height:'400px',color:'white'
   }
  }>
    <List>
      <h3 style={{fontFamily:"Montserrat",color:'#093545'}}>Membres</h3>
        <ListItem disablePadding>
            <ListItemButton bgcolor='#093545'>
            <ListItemText primary='Selena Roy'/>
            </ListItemButton>
        </ListItem>
        <Divider/>
        <ListItem disablePadding>
        <ListItemButton>
            <ListItemText primary='Emma Watson'/>
            </ListItemButton>
        </ListItem>
        <Divider/>

        <ListItem disablePadding>
        <ListItemButton>
            <ListItemText primary='Jhon Robert'/>
            </ListItemButton>
        </ListItem>
        <Divider/>
        <ListItem disablePadding>
        <ListItemButton>
            <ListItemText primary='Anne Hathaway'/>
            </ListItemButton>
        </ListItem>
        <Divider/>
        <ListItem disablePadding>
        <ListItemButton>
            <ListItemText primary='Ravi Shankar'/>
            </ListItemButton>
        </ListItem>
        <Divider/>
    
    </List>
    <Stack spacing={0}>
      <Pagination count={3} color="primary"  />
    </Stack>

  </Box>
  
  )
}
