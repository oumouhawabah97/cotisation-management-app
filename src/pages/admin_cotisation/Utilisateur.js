import React from 'react'
import { FaCircle } from 'react-icons/fa'
import { BiBlock } from 'react-icons/bi'
import { GrView } from 'react-icons/gr'
import { BiArchiveIn } from 'react-icons/bi'
// import swal from 'sweetalert'



import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { LinearProgress } from '@mui/material'

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData('Selena Roy', '01/01/2022', '300.000FCFA', <LinearProgress variant='determinate' value={100} color="success" />, 'Terminer'),
  createData(' Emma Watson', '01/01/2022', '300.000FCFA', <LinearProgress variant='determinate' value={100} color="success" />, 'Terminer'),
  createData('Jhon Robert', '01/01/2022', '300.000FCFA', <LinearProgress variant='determinate' value={60} color="info" />, 'Encours'),
  createData(' Anne Hathaway', '01/01/2022', '300.000FCFA', <LinearProgress variant='determinate' value={70} color="info" />, 'Encours'),
  createData('Ravi Shankar', '01/01/2022', '300.000FCFA', <LinearProgress variant='determinate' value={80} color="info" />, 'Encours'),
];




export default function Utilisateur() {
  // const mosalert=()=>{
  //   swal({
  //     title: "Good job!",
  //     text: "You clicked the button!",
  //     icon: "success",
  //     button: "Aww yiss!",
  //   });
  // }

  return (


    <div>
      <div className='row'>
        <div className='col-2'>

        </div>
        <div className='col-9'>
          <div class="row row-cols-1 row-cols-md-3 g-4 mt-4">
            <div class="col">
              <div class="card flex shadow mt-4" data-sr-id='7' >

                <div class="card-body ">
                  <p class="card-title" style={{ textAlign: 'start', fontFamily: 'Montserrat', fontWeight: 'bold', color: '#093545', fontSize: 13 }} >Membres Actif <FaCircle style={{ color: '#20DF7F' }} /></p>
                  <p style={{ textAlign: 'start', fontFamily: 'Montserrat', fontWeight: 'bold', color: '#093545', fontSize: 18 }}>94 Membres</p>
                  <p className='sel ' style={{ textAlign: 'start', fontFamily: 'Montserrat', color: '#093545', fontSize: 10 }} >Total record:18</p>
                </div>
              </div>
            </div>
            <div class="col">
              <div class="card flex shadow mt-4 " data-sr-id='7' >

                <div class="card-body ">
                  <p class="card-title" style={{ textAlign: 'start', fontFamily: 'Montserrat', fontWeight: 'bold', color: '#093545', fontSize: 13 }}>Membres Bloqués<BiBlock style={{ color: '#FF0000', fontSize: 14 }} />
                  </p>
                  <p style={{ textAlign: 'start', color: '#093545', fontFamily: 'Montserrat', fontWeight: 'bold', fontSize: 18 }}>6 Membres</p><br />

                </div>
              </div>
            </div>
            <div class="col">
              <div class="card flex shadow mt-4" data-sr-id='7'>

                <div class="card-body  " >
                  <p class="card-title" style={{ textAlign: 'start', fontFamily: 'Montserrat', fontWeight: 'bold', color: '#093545', fontSize: 13 }}>Total Caisse</p>
                  <p style={{ textAlign: 'start', color: '#093545', fontFamily: 'Montserrat', fontWeight: 'bold', fontSize: 18 }}>100 Membres</p><br />

                </div>
              </div>

              <div className=' ml-auto '>
                <button type="button" class="btn mt-1 " style={{ background: '#093545', color: '#fff', marginLeft: '240px' }}>Ajouter</button>
              </div>

            </div>


          </div>

          <TableContainer component={Paper} >
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead style={{ background: '#20DF7F' }}>
                <TableRow>
                  <TableCell align="center" style={{ fontSize: 18, fontFamily: 'Montserrat' }}>Membres</TableCell>
                  <TableCell align="center" style={{ fontSize: 18, fontFamily: 'Montserrat' }}>Date Debut</TableCell>
                  <TableCell align="center" style={{ fontSize: 18, fontFamily: 'Montserrat' }}>Seuil</TableCell>
                  <TableCell align="center" style={{ fontSize: 18, fontFamily: 'Montserrat' }}>Progression</TableCell>
                  <TableCell align="center" style={{ fontSize: 18, fontFamily: 'Montserrat' }}>Statut</TableCell>
                  <TableCell align="center" style={{ fontSize: 18, fontFamily: 'Montserrat' }}>Actions </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows.map((row) => (
                  <TableRow
                    key={row.name}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row" align="center" style={{ fontSize: 14, fontFamily: 'Montserrat' }}>
                      {row.name}
                    </TableCell>
                    <TableCell align="center" style={{ fontSize: 14, fontFamily: 'Montserrat' }}>{row.calories}</TableCell>
                    <TableCell align="center" style={{ fontSize: 14, fontFamily: 'Montserrat' }}>{row.fat}</TableCell>
                    <TableCell align="center" style={{ fontSize: 14, fontFamily: 'Montserrat' }}>{row.carbs}</TableCell>
                    <TableCell align="center" style={{ fontSize: 14, fontFamily: 'Montserrat' }}>{row.protein}

                    </TableCell>
                    <TableCell align="center"><BiArchiveIn style={{ fontSize: 16, margin: 3 }} /><GrView style={{ fontSize: 16, margin: 3 }} /><BiBlock style={{ fontSize: 16, margin: 3 }} /></TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>



          <div className='pigination mt-2'>
            <ul class="pagination d-flex justify-content-center flex-wrap pagination-rounded-flat pagination-success"
              style={{
                display: 'flex',
                paddingLeft: 0,
                listStyle: 'none',
                borderRadius: 0.25
              }} >

              <li class="page-item"><a class="page-link" href="/" style={{ color: 'black' }}>Previous Page</a></li>
              <li class="page-item" style={{ border: 'none' }}>
                <a class="page-link" href="/" data-abc="true"
                  style={{
                    color: 'black', border: 'none',
                    borderRadius: 50
                  }}>
                  <i class="fa fa-angle-left"></i></a></li>
              <li class="page-item active" style={{ border: 'none' }}>
                <a class="page-link" href="/" data-abc="true"
                  style={{
                    color: 'white',
                    border: 'none',
                    background: '#20DF7F',
                    borderRadius: 50
                  }} >1</a></li>

              <li class="page-item" style={{ border: 'none' }}>
                <a class="page-link" href="/" data-abc="true"
                  style={{
                    color: 'black', border: 'none',
                    borderRadius: 50
                  }}>2</a>
              </li>

              <li class="page-item"><a class="page-link" href="/" data-abc="true"
                style={{
                  color: 'black', border: 'none',
                  borderRadius: 50
                }}
              ><i class="fa fa-angle-right"></i></a></li>
              <li class="page-item"><a class="page-link" href="/" style={{ color: 'black' }}>Next Page</a></li>
            </ul>
          </div>

        </div>
      </div>
    </div>

  )
}

