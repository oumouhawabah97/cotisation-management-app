import React from 'react'
import {
  Box, 
  Grid,


} from '@mui/material'



import Input from '@mui/material/Input';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import InputAdornment from '@mui/material/InputAdornment';



import images2 from './images2.jpg'


const commonStyles = {
  bgcolor: 'background.paper',
  m: 1,
  borderColor: 'text.primary',
  width: '5rem',
  height: '20rem',
  marginLeft:'9rem',
  marginTop:-30
 

};



const Information = () => {




  return (
    <>






      <Grid container md={9} sx={{ m: '20px auto', minHeight: '50vh', boxShadow: 4, marginTop: 15 }}>
        

        <Grid item md={4} sx={{ background: '#fff' }}>
          
          <div className='col-6 mt-5 ml-5 m-5'>

            <p style={{ fontFamily: 'Montserrat', color: 'GrayText' }}> Profil</p>
            <img src={images2} class="card-img-top   pl-5" style={{ height: 90, width: 92, borderRadius: 600 }} alt='img' />
            <h3 style={{ fontFamily: 'Montserrat', fontSize: 26, color: '#093545' }}>Anta Gueye</h3>
            <h5 class="card-text" style={{ fontFamily: 'Montserrat' }}>admin</h5><br/>
            <button type="button" style={{ background: '#093545' , color:'white',fontFamily:'Montserrat', border:'none', borderRadius:5,fontSize:14}}>Changer votre photo</button>
            <Box sx={{ ...commonStyles, borderRight: 1 }} />
          </div>
          
        </Grid>
      
        <Grid item md={8} sx={{ width: '90vh' }}>
       


          &nbsp;
    

       
          <Grid gridRow='auto' sx={{ m: '0 auto', maxWidth: 548 }}>
          <Box sx={{ gridArea: 'header', bgcolor: '#20DF7F',textAlign:'justify' }}>Informatios Générales</Box>
            <FormControl fullWidth variant="standard">
              <InputLabel htmlFor="standard-adornment-prenom">Prenom</InputLabel>
              <Input
                id="standard-adornment-amount"
                endAdornment={<InputAdornment position="end">Ndiaga</InputAdornment>}
              />
            </FormControl>


          </Grid>
          
          <Grid gridRow='auto' sx={{ m: '0 auto', maxWidth: 548 }}>

            <FormControl fullWidth variant="standard">
              <InputLabel htmlFor="standard-adornment-prenom">Nom</InputLabel>
              <Input
                id="standard-adornment-amount"
                endAdornment={<InputAdornment position="end">Sall</InputAdornment>}
              />
            </FormControl>


          </Grid>

          
          <Grid gridRow='auto' sx={{ m: '0 auto', maxWidth: 548 }}>

            <FormControl fullWidth variant="standard">
              <InputLabel htmlFor="standard-adornment-prenom">Statut</InputLabel>
              <Input
                id="standard-adornment-amount"
                endAdornment={<InputAdornment position="end">Administrateur</InputAdornment>}
              />
            </FormControl>


          </Grid>

          
          <Grid gridRow='auto' sx={{ m: '0 auto', maxWidth: 548 }}>

            <FormControl fullWidth variant="standard">
              <InputLabel htmlFor="standard-adornment-prenom">E-mail</InputLabel>
              <Input
                id="standard-adornment-amount"
                endAdornment={<InputAdornment position="end">ndiaga@gmail.com</InputAdornment>}
              />
            </FormControl>


          </Grid>
          <Grid gridRow='auto' sx={{ m: '0 auto', maxWidth: 548 }}>
          <Box sx={{ gridArea: 'header', bgcolor: '#20DF7F',textAlign:'justify' }}>Statistiques</Box>
<FormControl fullWidth variant="standard">
  <InputLabel htmlFor="standard-adornment-prenom">Nombre de membre ajoutés</InputLabel>
  <Input
    id="standard-adornment-amount"
    endAdornment={<InputAdornment position="end">25</InputAdornment>}
  />
</FormControl>


</Grid>

<Grid gridRow='auto' sx={{ m: '0 auto', maxWidth: 548 }}>

<FormControl fullWidth variant="standard">
  <InputLabel htmlFor="standard-adornment-prenom">Nombre de membre archivés</InputLabel>
  <Input
    id="standard-adornment-amount"
    endAdornment={<InputAdornment position="end">10</InputAdornment>}
  />
</FormControl>


</Grid>

<Grid gridRow='auto' sx={{ m: '0 auto', maxWidth: 548 }}>

<FormControl fullWidth variant="standard">
  <InputLabel htmlFor="standard-adornment-prenom">Nombre de membre bloqués</InputLabel>
  <Input
    id="standard-adornment-amount"
    endAdornment={<InputAdornment position="end">4</InputAdornment>}
  />
</FormControl>


</Grid>

          
        

          &nbsp;
        </Grid>



      </Grid>










    </>
  )
}

export default Information;
