import React, { useState } from 'react'
import { Box, Button,
     Grid, Typography,
      useMediaQuery,
  
     } from '@mui/material'


import TextField from '@mui/material/TextField';


import { Link } from 'react-router-dom';
import PhoneTextField from "mui-phone-textfield";
import ListUsers from './ListUsers';






const Information = () => {

    const matches = useMediaQuery('(min-width:900px)');
    const [date, setDate] = useState('')
    const [email, setEmail] = useState('')
    const [prenom, setPrenom] = useState('')
    const [nom, setNom] = useState('')
    const [adresse, setAdresse] = useState('')
    const [profession, setProfession] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirpassword] = useState('')
    const [organisation, setOrganisation] = useState('')
    const [phoneNumber, setPhoneNumber] = useState(); // The PhoneNumber instance.

    const [valueNumber, setValuenumber] = useState(""); // The input value.
    const [country, setCountry] = useState("SN"); // The selected country.

    const [step, setStep] = useState(false)
    const [msgFailed, setMsgfailed] = useState(false)

    


    const onChangeNumber = ({ formattedValue, phoneNumber }) => {
        setValuenumber(formattedValue);
        setPhoneNumber(phoneNumber);
    };

    const onCountrySelect = ({ country, formattedValue, phoneNumber }) => {
        setValuenumber(formattedValue);
        setCountry(country);
        setPhoneNumber(phoneNumber);
        console.log(phoneNumber)
    };


    const handleForm = () => {
        setStep(true)
    }




    const checkPassword = async () => {
        try {
            if (password !== confirmPassword) {
                setStep(false)
                setMsgfailed(true);
                setPassword('');
                setConfirpassword('')
                return false
            } else {
                // await handleForm2()
            }
        } catch (err) {
            console.log(err)
        }
    }



    return (
        <>
         

      
            {
                matches ? (
                  
                    <Grid container md={9} sx={{ m: '20px auto', minHeight: '50vh', boxShadow: 4,marginTop:15 }}>
                       
                        <Grid item md={4} sx={{ background: '#fff' }}>
                     <h4 style={{fontFamily:'Montserrat'}}>Informations Générales</h4>
                        <ListUsers/>
                        </Grid>

                        <Grid item md={8} sx={{ width: '90vh' }}>
                           
                         

                            &nbsp;
                            <Grid gridRow='auto' sx={{ m: '0 auto', maxWidth: 548 }}>

                                <TextField

                                    label="Nom"
                                    value={nom}
                                    onChange={e => setNom(e.target.value)}
                                    variant="outlined"
                                    sx={{ borderRadius: 5, mr: 12, width: '25ch' }}
                                    size="small"
                                    InputLabelProps={{ shrink: true, required: true }}
                                />

                                <TextField
                                    label="Prénom"
                                    value={prenom}
                                    onChange={e => setPrenom(e.target.value)}
                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                    inputProps={{ style: { color: "black" } }}
                                    size="small"
                                    InputLabelProps={{ shrink: true, required: true }}
                                />


                            </Grid>

                            &nbsp;
                            <Grid gridRow='auto' sx={{ m: '0 auto', maxWidth: 548 }}>
                            {/* <Box sx={{ commonStyles, borderLeft: 1 }} /> */}

                                <TextField
                                    type='date'
                                    label="Date de naissance"
                                    value={date}
                                    onChange={e => setDate(e.target.value)}
                                    variant="outlined"
                                    sx={{ mr: 12, borderRadius: 5, width: '25ch' }}
                                    size="small"
                                    inputProps={{ InputProps: { min: "2022-04-17", max: "2023-05-04" } }}
                                    InputLabelProps={{ shrink: true, required: true }}
                                />


                                <TextField
                                    label="Profession"
                                    value={profession}
                                    onChange={e => setProfession(e.target.value)}
                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                    inputProps={{ style: { color: "black" } }}
                                    size="small"
                                    InputLabelProps={{ shrink: true, required: true }}
                                />


                            </Grid>

                            &nbsp;
                            <Grid gridRow='auto' sx={{ m: '0 auto', maxWidth: 548 }}>

                                <TextField
                                    type='password'
                                    label="Mot de passe"
                                    value={password}
                                    onChange={e => setPassword(e.target.value)}
                                    variant="outlined"
                                    sx={{ mr: 12, borderRadius: 5, width: '25ch' }}
                                    size="small"
                                    InputLabelProps={{ shrink: true, required: true }}
                                />

                                <TextField
                                    type='password'
                                    label="Confirme mot de passe"
                                    value={confirmPassword}
                                    onChange={e => setConfirpassword(e.target.value)}
                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                    inputProps={{ style: { color: "black" } }}
                                    size="small"
                                    InputLabelProps={{ shrink: true, required: true }}
                                />

                                {/* </Box> */}
                            </Grid>

                            &nbsp;
                            <Grid gridRow='auto' sx={{ m: '0 auto', maxWidth: 548 }}>

                                <TextField
                                    type='email'
                                    label="Email"
                                    value={email}
                                    onChange={e => setEmail(e.target.value)}
                                    variant="outlined"
                                    sx={{ mr: 12, borderRadius: 5, width: '25ch' }}
                                    size="small"
                                    InputLabelProps={{ shrink: true, required: true }}
                                />

                                <PhoneTextField
                                    label="Phone number"
                                    error={Boolean(valueNumber && phoneNumber?.country !== country)}
                                    value={valueNumber}
                                    country={country}
                                    onCountrySelect={onCountrySelect}
                                    onChange={onChangeNumber}
                                    sx={{ color: 'white', borderRadius: 5, width: '25ch', }}
                                    size="small"
                                    required
                                />

                              
                            </Grid>

                            &nbsp;
                            <Grid gridRow='auto' sx={{ m: '0 auto', maxWidth: 548 }}>

                                <TextField
                                    type='text'
                                    label="Adresse"
                                    value={adresse}
                                    onChange={e => setAdresse(e.target.value)}
                                    variant="outlined"
                                    sx={{ mr: 12, borderRadius: 5, width: '25ch' }}
                                    size="small"
                                    InputLabelProps={{ shrink: true, required: true }}
                                />

                                <TextField
                                    label="Organisation"
                                    value={organisation}
                                    onChange={e => setOrganisation(e.target.value)}
                                    sx={{ color: 'white', width: '25ch', height: '31px' }}
                                    inputProps={{ style: { color: "black" } }}
                                    size="small"
                                    InputLabelProps={{ shrink: true, required: true }}
                                />


                              
                            </Grid>



                            <Box
                                component="form"
                                sx={{
                                    '& > :not(style)': { width: '38ch' },
                                }}
                                noValidate
                                autoComplete="off"
                                style={{ maxWidth: '36ch', margin: '15px auto' }}
                            >


                                {

                                    (!nom || !prenom || !password || !confirmPassword ||
                                        !adresse || !organisation || !profession || !valueNumber) ? (


                                        <Button
                                            variant="contained"
                                            // onClick={() => handleForm2()}
                                            disabled
                                            sx={{
                                                maxWidth: '30vh',
                                                fontFamily: 'Montserrat',
                                                background: '#20df7f',
                                                boxShadow: 8,
                                                ':hover': {
                                                    bgcolor: '#00fe66', // theme.palette.primary.main
                                                    color: 'white',
                                                    background: '#093545'

                                                },
                                            }}
                                        >
                                            Mettre a jour
                                        </Button>
                                    ) : (
                                        <Button
                                            variant="contained"
                                            onClick={() => { checkPassword() }}
                                            sx={{
                                                maxWidth: '50vh',
                                                fontFamily: 'Montserrat',
                                                background: '#20df7f',
                                                boxShadow: 8,
                                                ':hover': {
                                                    bgcolor: '#00fe66', // theme.palette.primary.main
                                                    color: 'white',
                                                    background: '#093545',

                                                },
                                            }}

                                        >
                                            NEXT
                                        </Button>
                                    )

                                }

                            </Box>

                    

                        </Grid>


                    </Grid>
                ) : (
                    <Grid container  >


                        <Grid item md={6} sx={{ m: '100px auto' }}>

                            <Box
                                // component="form"
                                sx={{
                                    '& .MuiTextField-root': { m: 1 },
                                    m: '80px auto',
                                    // background:'#093545'
                                }}
                                noValidate
                                autoComplete="off"
                            >
                                <Typography variant='h5' align='center' mt={4} sx={{ color: 'black' }}>INSCRIVEZ-VOUS</Typography>


                                {
                                    !step ? (
                                        <>
                                            <div style={{ flexDirection: 'column', justifyContent: 'center', }}>

                                                <TextField

                                                    label="Nom"
                                                    value={nom}
                                                    onChange={e => setNom(e.target.value)}
                                                    type='text'
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                />
                                            </div>

                                            <div style={{ flexDirection: 'column' }}>

                                                <TextField
                                                    label="Prénom"
                                                    value={prenom}
                                                    onChange={e => setPrenom(e.target.value)}
                                                    type='text'
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                />
                                            </div>

                                            <div style={{ flexDirection: 'column' }}>

                                                <TextField
                                                    type='date'
                                                    label="Date de naissance"
                                                    value={date}
                                                    onChange={e => setDate(e.target.value)}
                                                    variant="outlined"
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                />
                                            </div>


                                            <div style={{ flexDirection: 'column' }}>

                                                <TextField
                                                    type='password'
                                                    label="Mot de passe"
                                                    value={password}
                                                    onChange={e => setPassword(e.target.value)}
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                />
                                            </div>



                                            <div style={{ flexDirection: 'column' }}>

                                                <TextField
                                                    type='password'
                                                    label="Confirme mot de passe"
                                                    value={confirmPassword}
                                                    onChange={e => setConfirpassword(e.target.value)}
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                    error={msgFailed}
                                                />

                                            </div>





                                        </>
                                    ) : (

                                        <>

                                            <div style={{ flexDirection: 'column' }}>

                                                <TextField
                                                    type='email'
                                                    label="Email"
                                                    value={email}
                                                    onChange={e => setEmail(e.target.value)}
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                />
                                            </div>



                                            <div style={{ flexDirection: 'column' }}>
                                                <PhoneTextField
                                                    label="Phone number"
                                                    error={Boolean(valueNumber && phoneNumber?.country !== country)}
                                                    value={valueNumber}
                                                    country={country}
                                                    onCountrySelect={onCountrySelect}
                                                    onChange={onChangeNumber}
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                    required
                                                />
                                            </div>


                                            <div style={{ flexDirection: 'column' }}>
                                                <TextField
                                                    label="Profession"
                                                    type='text'
                                                    value={profession}
                                                    onChange={e => setProfession(e.target.value)}
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                    required
                                                />
                                            </div>

                                            <div style={{ flexDirection: 'column' }}>
                                                <TextField
                                                    type='text'
                                                    label="Adresse"
                                                    value={adresse}
                                                    onChange={e => setAdresse(e.target.value)}
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                    required
                                                />
                                            </div>

                                            <div style={{ flexDirection: 'column' }}>
                                                <TextField
                                                    label="Organisation"
                                                    type='text'
                                                    value={organisation}
                                                    onChange={e => setOrganisation(e.target.value)}
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                    required
                                                />
                                            </div>



                                        </>
                                    )

                                }

                                {
                                    // msgFailed && <Typography variant='body2' mt color='error'>error</Typography>
                                }
                                {/* </div> */}

                                <Box
                                    // component="form"
                                    sx={{
                                        '& > :not(style)': { width: '25ch' },
                                    }}
                                    noValidate
                                    autoComplete="off"
                                    style={{ margin: '15px auto' }}
                                >

                                    {
                                        step ? (
                                            <Button
                                                variant="contained"
                                                sx={{
                                                    maxWidth: '50vh',
                                                    fontFamily: 'Montserrat',
                                                    background: '#20df7f',
                                                    boxShadow: 8,
                                                    m: '0 40px',
                                                    ':hover': {
                                                        bgcolor: '#00fe66', // theme.palette.primary.main
                                                        color: 'white',
                                                        background: '#093545',

                                                    },
                                                }}
                                            >
                                                Connexion
                                            </Button>
                                        ) : (
                                            (!nom || !prenom || !password || !confirmPassword) ? (
                                                <Button
                                                    variant="contained"
                                                    onClick={() => handleForm()}
                                                    disabled
                                                    sx={{
                                                        maxWidth: '50vh',
                                                        fontFamily: 'Montserrat',
                                                        background: '#fff',
                                                        boxShadow: 8,
                                                        border: '2px white solid',
                                                        m: '0 40px',
                                                        ':hover': {
                                                            bgcolor: '#00fe66', // theme.palette.primary.main
                                                            color: 'white',
                                                            background: '#093545',

                                                        },
                                                    }}

                                                >
                                                    NEXT
                                                </Button>
                                            ) : (
                                                <Button
                                                    variant="contained"
                                                    onClick={() => { checkPassword() }}
                                                    sx={{
                                                        maxWidth: '50vh',
                                                        fontFamily: 'Montserrat',
                                                        background: '#20df7f',
                                                        boxShadow: 8,
                                                        m: '0 40px',
                                                        ':hover': {
                                                            bgcolor: '#00fe66', // theme.palette.primary.main
                                                            color: 'white',
                                                            background: '#093545',

                                                        },
                                                    }}

                                                >
                                                    NEXT
                                                </Button>
                                            )
                                        )
                                    }
                                    &nbsp;
                                    <Typography variant='body2' sx={{ fontFamily: 'Montserrat', color: "black", m: '10px auto' }}  >
                                        Vous n'avez pas de compte,
                                        <strong>
                                            <Link to="/" style={{ textDecoration: 'none', color: '#093545', fontWeight: 'bold' }}>Connectez-vous</Link>
                                        </strong>
                                    </Typography>
                                </Box>
                                &nbsp;

                            </Box>

                        </Grid>




                    </Grid>
                )
            }

        </>
    )
}

export default Information;