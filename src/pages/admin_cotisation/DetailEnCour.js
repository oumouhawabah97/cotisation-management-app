import React from 'react'
import { Link } from 'react-router-dom'

export default function DetailEnCour() {
    return (
        <div>
    <div className='row'>
<div className='col-2'>
</div>

<center className="tete mt-5">
<div className='col-4 mt-5 '>
<p className='para ' style={{ textAlign: 'start', fontSize: 30 ,fontFamily:'Montserrat'}}>Emma Watson</p>
<p style={{textAlign:'start' , marginLeft:90 ,fontFamily:'Montserrat'}}>Comptable</p>
<div >
        
       <table class="table table-flex shadow mt-5">
       <thead className='flex' style={{ backgroundColor: '#093545',boxShadow:5 ,color:'white'}}>
            <tr style={{ height: 70, fontFamily: 'montserrat', fontSize: 18, margin: 10,boxShadow:4 }} >
              <th scope="col-2 " style={{ textAlign: 'center', fontFamily: 'Montserrat' , padding: 10 }} >Mois</th>
              <th scope="col-2" style={{ textAlign: 'center', fontFamily: 'Montserrat' , padding: 10}} >Date Début</th>
              <th scope="col-2" style={{ textAlign: 'center', fontFamily: 'Montserrat' , padding: 10}} >Montant</th>
            </tr>
          </thead>
    <tbody>
          
    
      <tr class="table-active shadow" style={{border:'4px solid white'}}>
      <td style={{ textAlign: 'inherit', fontFamily: 'Montserrat', padding: 10}}>Janvier</td>
              <td style={{ textAlign: 'center', fontFamily: 'Montserrat' , padding: 10}}>20/05/2022</td>
              <td style={{ textAlign: 'center', fontFamily: 'Montserrat' , padding: 10}}>40.000FCFA</td>
      </tr>
      <tr class="table-active" style={{border:'4px solid white'}}>
      <td  style={{ textAlign: 'inherit', fontFamily: 'Montserrat', padding: 10 }}>Fevrier</td>
              <td  style={{ textAlign: 'center', fontFamily: 'Montserrat', padding: 10 }}>25/05/2022</td>
              <td  style={{ textAlign: 'center', fontFamily: 'Montserrat' , padding: 10}}>35.000FCFA</td>
      </tr>
      <tr class="table-active" style={{border:'4px solid white'}}>
      <td  style={{ textAlign: 'inherit', fontFamily: 'Montserrat', padding: 10 }} >Mars</td>
              <td  style={{ textAlign: 'center', fontFamily: 'Montserrat' , padding: 10}}>30/05/2022</td>
              <td  style={{ textAlign: 'center', fontFamily: 'Montserrat', padding: 10 }}>20.000FCFA</td>
      </tr>
      <tr class="table-active" style={{border:'4px solid white'}}>
      <td  style={{ textAlign: 'inherit', fontFamily: 'Montserrat' , padding: 10}}>Avriel</td>
              <td  style={{ textAlign: 'center', fontFamily: 'Montserrat', padding: 10 }}>25/05/2022</td>
              <td  style={{ textAlign: 'center', fontFamily: 'Montserrat' , padding: 10}}>30.000FCFA</td>
      </tr>
      <tr class="table-active" style={{border:'4px solid white'}}>
      <td style={{ textAlign: 'inherit', fontFamily: 'Montserrat', padding: 10 }}>Juin</td>
              <td style={{ textAlign: 'center', fontFamily: 'Montserrat' , padding: 10}}>20/05/2022</td>
              <td style={{ textAlign: 'center', fontFamily: 'Montserrat' , padding: 10}}></td>
      </tr>
      <tr class="table-active" style={{border:'4px solid white'}}>
      <td  style={{ textAlign: 'inherit', fontFamily: 'Montserrat', padding: 10 }} >Juillet</td>
              <td  style={{ textAlign: 'center', fontFamily: 'Montserrat' , padding: 10}}>30/05/2022</td>
              <td  style={{ textAlign: 'center', fontFamily: 'Montserrat', padding: 10}}></td>
      </tr>
      
     
    </tbody>
  </table>
  <h2 style={{textAlign:'end',fontFamily:'Montserrat',color:'#093545',fontSize:18}}>Total=125.000FCFA</h2>
  <Link to='/admin/archiver'>
  <button type="button" className="btn btn-secondary" style={{background: '#093545'}}>Retour</button>
  </Link>
    </div>
    

    







</div>
</center>
</div>

</div>
    )
}
