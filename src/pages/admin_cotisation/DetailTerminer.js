import React from 'react'




import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
// import { Box } from '@mui/material'
import { LinearProgress } from '@mui/material'

function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData('Selena Roy', '01/01/2022', '300.000FCFA', <LinearProgress variant='determinate' value={100} color="success" />, 'Terminer'),
  createData(' Emma Watson', '01/01/2022', '300.000FCFA', <LinearProgress variant='determinate' value={100} color="success" />, 'Terminer'),
  createData('Jhon Robert', '01/01/2022', '300.000FCFA', <LinearProgress variant='determinate' value={60} color="info" />, 'Encours'),
  createData(' Anne Hathaway', '01/01/2022', '300.000FCFA', <LinearProgress variant='determinate' value={70} color="info" />, 'Encours'),
  createData('Ravi Shankar', '01/01/2022', '300.000FCFA', <LinearProgress variant='determinate' value={80} color="info" />, 'Encours'),
];
export default function Utilisateur() {
  return (


    <div>
      <div className='row'>
        <div className='col-3'>

        </div>
        <div className='col-4'>
              <h2>Selena Roy</h2>
              <p>Designer</p>
         
          <TableContainer component={Paper} style={{marginTop:80}} >
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead style={{ background: '#093545' }}>
                <TableRow>
                  <TableCell align="center" style={{ fontSize: 14, fontFamily: 'Montserrat' ,color:'white' }}>Mois</TableCell>
                  <TableCell align="center" style={{ fontSize: 14, fontFamily: 'Montserrat'  ,color:'white'}}>Date </TableCell>
                  <TableCell align="center" style={{ fontSize: 14, fontFamily: 'Montserrat'  ,color:'white'}}>Montant</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {rows.map((row) => (
                  <TableRow
                    key={row.name}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row" align="center" style={{ fontSize: 14, fontFamily: 'Montserrat' }}>
                      {row.name}
                    </TableCell>
                    <TableCell align="center" style={{ fontSize: 12, fontFamily: 'Montserrat' }}>{row.calories}</TableCell>
                    <TableCell align="center" style={{ fontSize: 12, fontFamily: 'Montserrat' }}>{row.fat}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <p style={{textAlign:'end', color:'#20DF7F'}}>Total:300.000FCFA</p>



       

        </div>
      </div>
    </div>

  )
}

