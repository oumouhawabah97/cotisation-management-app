import React, { useState } from 'react'
import {
    Box, Button,
    Grid, Typography,
    useMediaQuery,

} from '@mui/material'


import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';


import { Link } from 'react-router-dom';
import PhoneTextField from "mui-phone-textfield";

import images2 from './images2.jpg'






const Information = () => {

    const matches = useMediaQuery('(min-width:900px)');
    const [date, setDate] = useState('')
    const [email, setEmail] = useState('')
    const [prenom, setPrenom] = useState('')
    const [nom, setNom] = useState('')
    const [adresse, setAdresse] = useState('')
    const [profession, setProfession] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirpassword] = useState('')
    const [organisation, setOrganisation] = useState('')
    const [phoneNumber, setPhoneNumber] = useState(); // The PhoneNumber instance.

    const [valueNumber, setValuenumber] = useState(""); // The input value.
    const [country, setCountry] = useState("SN"); // The selected country.

    const [step, setStep] = useState(false)
    const [msgFailed, setMsgfailed] = useState(false)




    const onChangeNumber = ({ formattedValue, phoneNumber }) => {
        setValuenumber(formattedValue);
        setPhoneNumber(phoneNumber);
    };

    const onCountrySelect = ({ country, formattedValue, phoneNumber }) => {
        setValuenumber(formattedValue);
        setCountry(country);
        setPhoneNumber(phoneNumber);
        console.log(phoneNumber)
    };


    const handleForm = () => {
        setStep(true)
    }

    const commonStyles = {
        bgcolor: 'background.paper',
        m: 1,
        borderColor: 'text.primary',
        width: '5rem',
        height: '20rem',
        marginLeft:'9rem',
        marginTop:-30
       

    };

    const checkPassword = async () => {
        try {
            if (password !== confirmPassword) {
                setStep(false)
                setMsgfailed(true);
                setPassword('');
                setConfirpassword('')
                return false
            } else {

            }
        } catch (err) {
            console.log(err)
        }
    }



    return (
        <>



            {
                matches ? (

                    <Grid container md={9} sx={{ m: '20px auto', minHeight: '50vh', boxShadow: 4, marginTop: 15 }}>

                        <Grid item md={4} sx={{ background: '#fff' }}>

                            <div className='col-6 mt-5 ml-5 m-5'>


                                <p style={{ fontFamily: 'Montserrat', color: 'GrayText' }}> Profil</p>
                                <img src={images2} class="card-img-top   pl-5" style={{ height: 90, width: 92, borderRadius: 600 }} alt='img' />
                                <h3 style={{ fontFamily: 'Montserrat', fontSize: 26, color: '#093545' }}>Anta Gueye</h3>
                                <h5 class="card-text" style={{ fontFamily: 'Montserrat' }}>admin</h5>

                                <button type="button" style={{ background: '#093545', color: 'white', fontFamily: 'Montserrat', border: 'none', borderRadius: 3, marginTop: 20, fontSize: 14 }}>
                                    Changer votre photo</button>
                                    <Box sx={{ ...commonStyles, borderRight: 1 }} />
                            </div>
                          
                        </Grid>

                      
                        <Grid item md={8} sx={{ width: '90vh' }}>
                       
                            <h4 style={{ textAlign: 'justify' , fontFamily: 'Montserrat',marginTop:30}}>  Information Générale</h4>

                            &nbsp;
                            {/* <Grid gridRow='auto' sx={{ m: '5 auto', maxWidth: 548 }}>
                                

                                <TextField

                                    label="Nom"
                                    value={nom}
                                    onChange={e => setNom(e.target.value)}
                                    variant="outlined"
                                    sx={{ borderRadius: 5, mr: 18, width: '20ch' }}
                                    inputProps={{ style: { color: "black" , height:6 } }}
                                    size="small"
                                    InputLabelProps={{ shrink: true, required: true }}
                                />

                                <TextField
                                    label="Prénom"
                                    value={prenom}
                                    onChange={e => setPrenom(e.target.value)}
                                    sx={{ color: 'white', borderRadius: 5, width: '20ch', mr: 6}}
                                    inputProps={{ style: { color: "black" , height:6 } }}
                                    size="small"
                                    InputLabelProps={{ shrink: true, required: true }}
                                />


                            </Grid> */}
                             <Grid gridRow='auto' sx={{ m: '30 auto', maxWidth: 400 }}>

<TextField
    label="Nom"
    value={phoneNumber}
    onChange={e => setPhoneNumber(e.target.value)}
    sx={{ color: 'white', borderRadius: 5, width: '45ch', mr: 20 }}
    inputProps={{ style: { color: "black" , height:6} }}
    size="small"
    InputLabelProps={{ shrink: true, required: true }}
/>

</Grid>
&nbsp;&nbsp;
                             <Grid gridRow='auto' sx={{ m: '30 auto', maxWidth: 400 }}>

<TextField
    label="Prenom"
    value={phoneNumber}
    onChange={e => setPhoneNumber(e.target.value)}
    sx={{ color: 'white', borderRadius: 5, width: '45ch', mr: 20 }}
    inputProps={{ style: { color: "black" , height:6} }}
    size="small"
    InputLabelProps={{ shrink: true, required: true }}
/>

</Grid>

                            &nbsp;
                            <Grid gridRow='auto' sx={{ m: '30 auto', maxWidth: 400 }}>

                                <TextField
                                    label="Téléphone"
                                    value={phoneNumber}
                                    onChange={e => setPhoneNumber(e.target.value)}
                                    sx={{ color: 'white', borderRadius: 5, width: '45ch', mr: 20 }}
                                    inputProps={{ style: { color: "black" , height:6} }}
                                    size="small"
                                    InputLabelProps={{ shrink: true, required: true }}
                                />

                            </Grid>




                            &nbsp;


                            &nbsp;
                            <Grid gridRow='auto' sx={{ m: '30 auto', maxWidth: 400 }}>

                                <TextField
                                    label="Email"
                                    value={email}
                                    onChange={e => setEmail(e.target.value)}
                                    sx={{ color: 'white', borderRadius: 5, width: '45ch', mr: 20 }}
                                    inputProps={{ style: { color: "black" , height:6 } }}
                                    size="small"
                                    InputLabelProps={{ shrink: true, required: true }}
                                />

                            </Grid>
                            &nbsp;
                            <Grid gridRow='auto' sx={{ m: '30 auto', maxWidth: 400 }}>

                                <TextField
                                    label="Statut"
                                    value='admin'
                                    onChange={e => setProfession(e.target.value)}
                                    sx={{ color: 'white', borderRadius: 5, width: '45ch', mr: 20 }}
                                    inputProps={{ style: { color: "black" , height:6 } }}
                                    size="small"
                                    InputLabelProps={{ shrink: true, required: true }}
                                />

                            </Grid>



                            &nbsp;








                            &nbsp;



                            <Box
                                component="form"
                                sx={{
                                    '& > :not(style)': { width: '38ch' },
                                }}
                                noValidate
                                autoComplete="off"
                                style={{ maxWidth: '36ch', margin: '15px auto' }}
                            >


                                {

                                    (!nom || !prenom || !date || !password || !confirmPassword ||
                                        !adresse || !organisation || !profession || !valueNumber) ? (


                                        <Button variant="contained" sx={{
                                            maxWidth: '28vh',  fontSize: 10, color: 'white',marginRight:8,
                                            background: '#093545', fontFamily: 'Montserrat'
                                        }}>Mettre a jour</Button>

                                    ) : (
                                        <Button
                                            variant="contained"
                                            onClick={() => { checkPassword() }}
                                            sx={{
                                                maxWidth: '50vh',
                                                fontFamily: 'Montserrat',
                                                background: '#20df7f',
                                                boxShadow: 8,
                                                ':hover': {
                                                    bgcolor: '#00fe66', // theme.palette.primary.main
                                                    color: 'white',
                                                    background: '#093545',

                                                },
                                            }}

                                        >
                                            NEXT
                                        </Button>
                                    )

                                }

                            </Box>



                        </Grid>


                    </Grid>
                ) : (
                    <Grid container  >


                        <Grid item md={6} sx={{ m: '100px auto' }}>

                            <Box
                                // component="form"
                                sx={{
                                    '& .MuiTextField-root': { m: 1 },
                                    m: '80px auto',
                                    // background:'#093545'
                                }}
                                noValidate
                                autoComplete="off"
                            >
                                <Typography variant='h5' align='center' mt={4} sx={{ color: 'black' }}>INSCRIVEZ-VOUS</Typography>


                                {
                                    !step ? (
                                        <>
                                            <div style={{ flexDirection: 'column', justifyContent: 'center', }}>

                                                <TextField

                                                    label="Nom"
                                                    value={nom}
                                                    onChange={e => setNom(e.target.value)}
                                                    type='text'
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                />
                                            </div>

                                            <div style={{ flexDirection: 'column' }}>

                                                <TextField
                                                    label="Prénom"
                                                    value={prenom}
                                                    onChange={e => setPrenom(e.target.value)}
                                                    type='text'
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                />
                                            </div>












                                        </>
                                    ) : (

                                        <>

                                            <div style={{ flexDirection: 'column' }}>

                                                <TextField
                                                    type='email'
                                                    label="Email"
                                                    value={email}
                                                    onChange={e => setEmail(e.target.value)}
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                />
                                            </div>



                                            <div style={{ flexDirection: 'column' }}>
                                                <PhoneTextField
                                                    label="Phone number"
                                                    error={Boolean(valueNumber && phoneNumber?.country !== country)}
                                                    value={valueNumber}
                                                    country={country}
                                                    onCountrySelect={onCountrySelect}
                                                    onChange={onChangeNumber}
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                    required
                                                />
                                            </div>


                                            <div style={{ flexDirection: 'column' }}>
                                                <TextField
                                                    label="Profession"
                                                    type='text'
                                                    value={profession}
                                                    onChange={e => setProfession(e.target.value)}
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                    required
                                                />
                                            </div>

                                            <div style={{ flexDirection: 'column' }}>
                                                <TextField
                                                    type='text'
                                                    label="Adresse"
                                                    value={adresse}
                                                    onChange={e => setAdresse(e.target.value)}
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                    required
                                                />
                                            </div>

                                            <div style={{ flexDirection: 'column' }}>
                                                <TextField
                                                    label="Organisation"
                                                    type='text'
                                                    value={organisation}
                                                    onChange={e => setOrganisation(e.target.value)}
                                                    sx={{ color: 'white', borderRadius: 5, width: '25ch' }}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small"
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                    required
                                                />
                                            </div>



                                        </>
                                    )

                                }

                                {
                                    // msgFailed && <Typography variant='body2' mt color='error'>error</Typography>
                                }
                                {/* </div> */}

                                <Box
                                    // component="form"
                                    sx={{
                                        '& > :not(style)': { width: '25ch' },
                                    }}
                                    noValidate
                                    autoComplete="off"
                                    style={{ margin: '15px auto' }}
                                >

                                    {
                                        step ? (
                                            <Button
                                                variant="contained"
                                                sx={{
                                                    maxWidth: '50vh',
                                                    fontFamily: 'Montserrat',
                                                    background: '#20df7f',
                                                    boxShadow: 8,
                                                    m: '0 40px',
                                                    ':hover': {
                                                        bgcolor: '#00fe66', // theme.palette.primary.main
                                                        color: 'white',
                                                        background: '#093545',

                                                    },
                                                }}
                                            >
                                                Connexion
                                            </Button>
                                        ) : (
                                            (!nom || !prenom || !date || !password || !confirmPassword) ? (
                                                <Button
                                                    variant="contained"
                                                    onClick={() => handleForm()}
                                                    disabled
                                                    sx={{
                                                        maxWidth: '50vh',
                                                        fontFamily: 'Montserrat',
                                                        background: '#fff',
                                                        boxShadow: 8,
                                                        border: '2px white solid',
                                                        m: '0 40px',
                                                        ':hover': {
                                                            bgcolor: '#00fe66', // theme.palette.primary.main
                                                            color: 'white',
                                                            background: '#093545',

                                                        },
                                                    }}

                                                >
                                                    NEXT
                                                </Button>
                                            ) : (
                                                <Button
                                                    variant="contained"
                                                    onClick={() => { checkPassword() }}
                                                    sx={{
                                                        maxWidth: '50vh',
                                                        fontFamily: 'Montserrat',
                                                        background: '#20df7f',
                                                        boxShadow: 8,
                                                        m: '0 40px',
                                                        ':hover': {
                                                            bgcolor: '#00fe66', // theme.palette.primary.main
                                                            color: 'white',
                                                            background: '#093545',

                                                        },
                                                    }}

                                                >
                                                    NEXT
                                                </Button>
                                            )
                                        )
                                    }
                                    &nbsp;
                                    <Typography variant='body2' sx={{ fontFamily: 'Montserrat', color: "black", m: '10px auto' }}  >
                                        Vous n'avez pas de compte,
                                        <strong>
                                            <Link to="/" style={{ textDecoration: 'none', color: '#093545', fontWeight: 'bold' }}>Connectez-vous</Link>
                                        </strong>
                                    </Typography>
                                </Box>
                                &nbsp;

                            </Box>

                        </Grid>




                    </Grid>
                )
            }

        </>
    )
}

export default Information;