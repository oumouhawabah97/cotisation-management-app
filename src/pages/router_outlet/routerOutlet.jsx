import { Outlet, useLocation } from 'react-router-dom'
import AdminDashboard from '../admin_dashboard/admin_dashboard'

const RouterOutlet = () => {
    
  const locationUrl = useLocation();
  const currentUrl = locationUrl.pathname.split("/").join("");

  return (
    <>
       {currentUrl==='admin' && <AdminDashboard />}
        <Outlet/>
    </>
  )
}

export default RouterOutlet