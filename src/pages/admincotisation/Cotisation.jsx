import React, { useEffect, useState } from 'react';

import { Grid, Card, CardContent, Typography, Button, CardActionArea, useMediaQuery, LinearProgress, Box } from '@mui/material'

import MenuCotisation from '../../components/menu_dashboard/menu_cotisation';
import ArrowUpwardOutlinedIcon from '@mui/icons-material/ArrowUpwardOutlined';
import CurrencyExchangeOutlinedIcon from '@mui/icons-material/CurrencyExchangeOutlined';
import TableCotiseAdmin from '../../components/table/table_cotise_admin';



const Cotisation = ()=>{
    const matches = useMediaQuery('(min-width:980px)');
    // const [open, setOpen] = React.useState(false);
    // const [anchorEl, setAnchorEl] = React.useState(null);
    // const [contribution, setcontribution] = useState("")
    const [allData, setallData] = useState('')
    // const openMenu = Boolean(anchorEl);

    const user = JSON.parse(localStorage.getItem("user"))
    const token = user.accessToken
    
    // const handleClick = (event) => {
    //   setAnchorEl(event.currentTarget);
    // };
    // const handleClose = () => {
    //   setAnchorEl(null);
    // };
  
   
  
    
  
    useEffect(() => {

        // const getContributions = () => {
        //     return fetch("https://cotisation-api-app.herokuapp.com/contributions", {
    
        //         method: 'GET',
        //         headers: {
        //             'Content-Type': 'application/json',
        //             'Accept': 'application/json',
        //             'Authorization': `Bearer ${token}`
        //         },
    
        //     }).then((res) => res.json())
        //         .then((res) => {
        //             setcontribution(res)
        //         })
        //         .catch((error) => {
        //         });
        // }

        const getAll=()=>{
            return fetch("https://cotisation-api-app.herokuapp.com/contributions/list/populate", {
    
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': `Bearer ${token}`
                },
    
            }).then((res) => res.json())
                .then((res) => {
                    setallData(res)
                })
                .catch((error) => {
                });
        }

        // getContributions()
        getAll();
    }, [token])
    
    
  
  
    
    return (
      <Grid 
          container md={12} sm={8} spacing={2} p={3}
              sx={{
                  border:'1px solid', 
                  // m:'50px auto',
                  display:'block',
                  maxHeight:50
                  
              }}
      >
              {
                  matches ?(
                      <>
                              <Grid item md={2} direction='row' mb={6}>
  
                              </Grid>
                            
                              <Grid 
                                  item md={8} 
                                  sx={{
                                      // border:'1px solid',
                                      display:{sm:'block', md:'flex'},
                                      m:'0px auto', 
                                      justifyContent:'center',
                                      
                                  }} 
                                  columnGap={3}
                                  
                                  >
  
                                  <Card sx={{minWidth:220,  boxShadow:4}}>
                                      <CardActionArea>
                                          <CardContent sx={{textAlign:'left', minWidth:200}}>
                                          <Typography gutterBottom variant="body2" sx={{fontFamily:'Montserrat'}} color="textSecondary" component="p">
                                              Juin 
                                          </Typography>
                                          <Typography  variant="body2" component="h5" sx={{fontFamily:'Montserrat'}} style={{fontWeight:'bold'}}>
                                              225 000 FCFA <Typography variant='caption' color='#20DF7F'>
                                                  +2,5%<ArrowUpwardOutlinedIcon fontSize='small' sx={{maxHeight:12}} />
                                              </Typography>
                                          </Typography>
                  
                                          <Typography  variant="subtitle1"  sx={{fontFamily:'Montserrat', fontSize:'12px'}}>
                                              Nombre de cotisation 27    
                                          </Typography>
                                          </CardContent>
                                      </CardActionArea>
                                  </Card>
  
                                  <Card sx={{minWidth:200, maxHeight:100, boxShadow:4}}>
                                      <CardActionArea>
                                          <CardContent sx={{textAlign:'left',minWidth:230}}>
                                              <Typography gutterBottom variant="body2" sx={{fontFamily:'Montserrat'}} color="textSecondary" component="p">
                                                  Mai 
                                                  
                                              </Typography>
                                              <Typography  variant="body2" component="h2" sx={{fontFamily:'Montserrat'}} style={{fontWeight:'bold'}}>
                                                  100 000 FCFA  
                                              </Typography>
                      
                                              <Typography  variant="subtitle1"  sx={{fontFamily:'Montserrat', fontSize:'12px'}}>
                                                  Nombre de cotisation 23    
                                              </Typography>
                                          
                                          </CardContent>
                                      </CardActionArea>
                                  </Card>
  
                                  <Card sx={{minWidth:350, maxHeight:100, boxShadow:4}}>
                                      <CardActionArea>
                                          <CardContent sx={{textAlign:'left', display:'flex'}}>
                                              <Box sx={{borderRadius:3, p:2,bgcolor:'#edfcf5'}}>
                                                  <CurrencyExchangeOutlinedIcon fontSize='large' sx={{width:50}}/>
                                              </Box>
                                              &nbsp;
                                              <div>
                                                  <Typography variant="body2" sx={{fontFamily:'Montserrat'}} color="textSecondary" component="p">
                                                      Caisse
                                                      
                                                  </Typography>
                                                  <Typography gutterBottom sx={{fontFamily:'Montserrat'}} variant="caption" component="span" >
                                                      3.500.000 FCFA / <b>5.000.000 FCFA</b>   
                                                  </Typography>
                                                  <Typography gutterBottom sx={{fontFamily:'Montserrat'}} variant="h5" component="h2" >
                                                      <Box sx={{width: '100%'}}>
                                                          <LinearProgress variant="determinate" value={80} sx={{background:'#20df7f'}} />      
                                                      </Box>
                                                  </Typography>
                                                     <Typography align='right' variant='body2'> +3%</Typography>
                                              </div>
                                          
                                          </CardContent>
                                      </CardActionArea>
                                  </Card>
  
                              </Grid>
  
                              <Typography mt={2}   sx={{ml:'63rem', maxWidth:'30vh'}} justifySelf>   
                                      {/* <Menubutton /> */}
                                      <MenuCotisation />
                              </Typography>
                              <Grid item md={7} sx={{m:'0px auto',  justifyContent:'center'}}>
                                  {/* <DataGridDemo users={null}/> */}
                                  <TableCotiseAdmin users={allData} />
                                  {/* <DataGrid
                                      getRowId={(rowsUser) => rowsUser._id}
                                      rows={contribution}
                                      columns={columns}
                                      pageSize={4}
                                      rowsPerPageOptions={[5]}
                                    //   checkboxSelection
                                      disableSelectionOnClick
                                      sx={{height:350}}
                                  /> */}
                              </Grid>
                              
                      </>        
                  ):(
                      <> 
                          <Grid item md={2} direction='row' mb={8}>
  
                          </Grid>
                          <Typography    sx={{ml:'10px', maxWidth:'30vh'}} justifySelf>
                              <Button 
                                  variant='contained' 
                                  sx={{
                                      background:'#20df7f', 
                                      maxWidth:55, 
                                      px:2,
                                      fontSize:10,
                                      color:'black',
                                      fontFamily:'Montserrat',    
                                      ':hover':{
                                          background:''
                                      }
                                  }}>
                                  Ajouter
  
                                  
                              </Button>
                              {/* &nbsp;
                              <Button
                                      sx={{
                                          background:'#e6e6e7',  
                                          fontFamily:'Montserrat',    
                                          color:'#20df7f',
                                          border:'1px solid transparent',
                                          maxHeight:25 
                                      }}
                                      title='Archives'
                                      aria-controls={open ? 'basic-menu' : undefined}
                                      aria-haspopup="true"
                                      aria-expanded={open ? 'true' : undefined}
                                      onClick={handleClick}
                                  >
                                     <LinearScaleIcon />
                              
                              </Button>
                                  <Menu
                                      id="basic-menu"
                                      anchorEl={anchorEl}
                                      open={openMenu}
                                      onClick={handleClose}
                                      MenuListProps={{
                                      'aria-labelledby': 'basic-button',
                                      }}
                                  >
                                      <MenuItem onClick={handleClose}>Archives</MenuItem>
                                      <MenuItem onClick={handleClose}>Utilisateurs bloqués</MenuItem>
                                      <MenuItem onClick={handleClose}>Cotisation términé</MenuItem>
                                  </Menu> */}
                          </Typography>
  
                          <Grid 
                              item md={8} 
                              sx={{
                                  // border:'1px solid',
                                  display:'block',
                                //   m:'0px auto', 
                                  
                                  m:'0 70px'
                              }} 
                              columnGap={2}
                              >
  
                              <Card sx={{maxWidth:200, boxShadow:8, mb:1}}>
                                  <CardActionArea>
                                      <CardContent sx={{textAlign:'left'}}>
                                      <Typography variant="body2" sx={{fontFamily:'Montserrat'}} color="textSecondary" component="p">
                                          Membres Actif
                                          
                                      </Typography>
                                      <Typography gutterBottom variant="h5" component="h2" sx={{fontFamily:'Montserrat'}}>
                                          94 Membres     
                                      </Typography>
                                      
                                      </CardContent>
                                  </CardActionArea>
                              </Card>
                              <Card sx={{maxWidth:200, boxShadow:8, mb:1}}>
                                  <CardActionArea>
                                      <CardContent sx={{textAlign:'left'}}>
                                          <Typography variant="body2" sx={{fontFamily:'Montserrat'}} color="textSecondary" component="p">
                                              Membres Bloqués
                                              
                                          </Typography>
                                          <Typography gutterBottom sx={{fontFamily:'Montserrat'}} variant="h5" component="h2" >
                                              6 Membres     
                                          </Typography>
                                      
                                      </CardContent>
                                  </CardActionArea>
                              </Card>
  
                              <Card sx={{maxWidth:200, boxShadow:8, mb:1}}>
                                  <CardActionArea>
                                      <CardContent sx={{textAlign:'left'}}>
                                      <Typography variant="body2" sx={{fontFamily:'Montserrat'}} color="textSecondary" component="p">
                                          Total Effectif
                                          
                                      </Typography>
                                      <Typography gutterBottom sx={{fontFamily:'Montserrat'}} variant="h5" component="h2" >
                                          100 Membres     
                                      </Typography>
                                      
                                      </CardContent>
                                  </CardActionArea>
                              </Card>
  
                          </Grid>
  
                          <Grid item md={7} sx={{m:'0px 40px',  justifyContent:'center'}}>
                              {/* <DataGridDemo/> */}
                              {/* <DataGrid
                                  rows={rows}
                                  columns={columns}
                                  pageSize={4}
                                  rowsPerPageOptions={[5]}
                                  checkboxSelection
                                  disableSelectionOnClick
                                  sx={{height:350}}
                              /> */}
                          </Grid>
                      </>
                  )
              }
      </Grid>
    )
}

export default Cotisation
