import React from 'react'
import { Grid, Card, CardContent, Typography, Button, CardActionArea, useMediaQuery, Menu, MenuItem, Box, LinearProgress } from '@mui/material'
import LinearScaleIcon from '@mui/icons-material/LinearScale';
import ArrowUpwardOutlinedIcon from '@mui/icons-material/ArrowUpwardOutlined';
import CurrencyExchangeOutlinedIcon from '@mui/icons-material/CurrencyExchangeOutlined';
import ProgresLineaire from '../../components/progressLineaire/progresseLineaire';
import ChartLineaire from '../../components/progressLineaire/chartLineaire';
// import Card from '@mui/material/Card';
import AdminTable from '../../components/table/admin_table';



const AdminDashboard = () => {

    const matches = useMediaQuery('(min-width:980px)');
    //   const [open, setOpen] = React.useState(false);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const openMenu = Boolean(anchorEl);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };







    const columns = [

        {
            field: 'membres',
            headerName: 'Membres',
            width: 130,
            headerAlign: 'left',

        },
        {
            field: 'montant',
            headerName: 'Montant',
            width: 130,

            headerAlign: 'left'
        },
        {
            field: 'date',
            headerName: 'Date',
            width: 130,

            headerAlign: 'left'
        },

        {
            field: 'status',
            headerName: 'Status',
            description: 'This column has a value getter and is not sortable.',

            width: 110,
            renderCell: () => (
                <>
                    <Button variant='outlined' size='small'>ok</Button>
                </>
            )
           
        },

    ];

    const columns_2 = [

        {
            field: 'membres',
            headerName: 'Membres',
            width: 130,
            headerAlign: 'left',

        },

        {
            field: 'date',
            headerName: 'Date',
            width: 130,

            headerAlign: 'left'
        },

        {
            field: 'progression',
            headerName: 'Progression',
            description: 'This column has a value getter and is not sortable.',
            width: 110,
            renderCell: () => (
                <>
                    <Button variant='outlined' size='small'>ok</Button>
                </>
            )
            //   valueGetter: (params) =>
            //     `${params.row.membres || ''} ${params.row.seuil || ''}`,
        },

    ];

    const rows = [
        { id: 1, membres: 'membres', montant: 'Snow', date: 'Jon', progression: 35, status: 'ok' },
        { id: 2, membres: 'membres', montant: 'Lannister', date: 'Cersei', progression: 42, status: 'ok' },
        { id: 3, membres: 'membres', montant: 'Lannister', date: 'Jaime', progression: 45, status: 'ok' },
        { id: 4, membres: 'membres', montant: 'Stark', date: 'Arya', progression: 16, status: 'ok' },
        { id: 5, membres: 'membres', montant: 'Targaryen', date: 'Daenerys', progression: null, status: 'ok' },
        { id: 6, membres: 'membres', montant: 'Melisandre', date: null, progression: 150, status: 'ok' },
        { id: 7, membres: 'membres', montant: 'Clifford', date: 'Ferrara', progression: 44, status: 'ok' },
        { id: 8, membres: 'membres', montant: 'Frances', date: 'Rossini', progression: 36, status: 'ok' },
        { id: 9, membres: 'membres', montant: 'Roxie', date: 'Harvey', progression: 65, status: 'ok' },
    ];

    const rows_2 = [
        { id: 1, membres: 'membres', date: 'Jon', progression: 35, status: 'ok' },
        { id: 2, membres: 'membres', date: 'Cersei', progression: 42, status: 'ok' },
        { id: 3, membres: 'membres', date: 'Jaime', progression: 45, status: 'ok' },
        { id: 4, membres: 'membres', date: 'Arya', progression: 16, status: 'ok' },
        { id: 5, membres: 'membres', date: 'Daenerys', progression: null, status: 'ok' },
        { id: 6, membres: 'membres', date: null, progression: 150, status: 'ok' },
        { id: 7, membres: 'membres', date: 'Ferrara', progression: 44, status: 'ok' },
        { id: 8, membres: 'membres', date: 'Rossini', progression: 36, status: 'ok' },
        { id: 9, membres: 'membres', date: 'Harvey', progression: 65, status: 'ok' },
    ];

    return (
        <Grid
            container md={12} sm={8}
            sx={{

                display: 'block',
                maxHeight: 50

            }}
        >
            {
                matches ? (
                    <Grid container m='0 auto' rowSpacing={2}>
                        <Grid item md={12} mt={6}>

                        </Grid>

                        <Grid
                            item md={10}
                            sx={{
                                // border:'1px solid',
                                display: { sm: 'block', md: 'flex' },
                                m: '0px auto',
                                justifyContent: 'center',

                            }}
                            columnGap={3}

                        >

                            <Card sx={{ minWidth: 220, boxShadow: 4 }}>
                                <CardActionArea>
                                    <CardContent sx={{ textAlign: 'left', minWidth: 200 }}>
                                        <Typography gutterBottom variant="body2" sx={{ fontFamily: 'Montserrat' }} color="textSecondary" component="p">
                                            Juin
                                        </Typography>
                                        <Typography variant="body2" component="h5" sx={{ fontFamily: 'Montserrat' }} style={{ fontWeight: 'bold' }}>
                                            225 000 FCFA <Typography variant='caption' color='#20DF7F'>
                                                +2,5%<ArrowUpwardOutlinedIcon fontSize='small' sx={{ maxHeight: 12 }} />
                                            </Typography>
                                        </Typography>

                                        <Typography variant="subtitle1" sx={{ fontFamily: 'Montserrat', fontSize: '12px' }}>
                                            Nombre de cotisation 27
                                        </Typography>
                                    </CardContent>
                                </CardActionArea>
                            </Card>

                            <Card sx={{ minWidth: 200, maxHeight: 100, boxShadow: 4 }}>
                                <CardActionArea>
                                    <CardContent sx={{ textAlign: 'left', minWidth: 230 }}>
                                        <Typography gutterBottom variant="body2" sx={{ fontFamily: 'Montserrat' }} color="textSecondary" component="p">
                                            Mai

                                        </Typography>
                                        <Typography variant="body2" component="h2" sx={{ fontFamily: 'Montserrat' }} style={{ fontWeight: 'bold' }}>
                                            100 000 FCFA
                                        </Typography>

                                        <Typography variant="subtitle1" sx={{ fontFamily: 'Montserrat', fontSize: '12px' }}>
                                            Nombre de cotisation 23
                                        </Typography>

                                    </CardContent>
                                </CardActionArea>
                            </Card>

                            <Card sx={{ minWidth: 350, maxHeight: 100, boxShadow: 4 }}>
                                <CardActionArea>
                                    <CardContent sx={{ textAlign: 'left', display: 'flex' }}>
                                        <Box sx={{ borderRadius: 3, p: 2, bgcolor: '#edfcf5' }}>
                                            <CurrencyExchangeOutlinedIcon fontSize='large' sx={{ width: 50 }} />
                                        </Box>
                                        &nbsp;
                                        <div>
                                            <Typography variant="body2" sx={{ fontFamily: 'Montserrat' }} color="textSecondary" component="p">
                                                Caisse

                                            </Typography>
                                            <Typography gutterBottom sx={{ fontFamily: 'Montserrat' }} variant="caption" component="span" >
                                                3.500.000 FCFA / <b>5.000.000 FCFA</b>
                                            </Typography>
                                            <Typography gutterBottom sx={{ fontFamily: 'Montserrat' }} variant="h5" component="h2" >
                                                <Box sx={{ width: '100%' }}>
                                                    <LinearProgress variant="determinate" value={80} sx={{ background: '#20df7f' }} />
                                                </Box>
                                            </Typography>
                                            <Typography align='right' variant='body2'> +3%</Typography>
                                        </div>

                                    </CardContent>
                                </CardActionArea>
                            </Card>

                        </Grid>


                        <Grid item md={10} columnGap={2} sx={{ m: '0px auto', display: 'flex' }} >

                            <Card sx={{ maxWidth: 600, ml: '60px', boxShadow: 2 }}>
                                <CardContent>
                                    {/* <CardHeader sx={{border:'1px black'}}> */}
                                    <Typography
                                        sx={{
                                            color: 'black',
                                            border: '1px solid',
                                            background: '#20DF7F',
                                            // minHeight:20,
                                            borderRadius: '20px 20px 0 0',
                                            p: 1,
                                            width: '90vh',
                                            m: '0 auto'

                                        }}
                                        align='center'
                                    >
                                        Evolution des cotisation en fonction du temps</Typography>
                                    {/* </CardHeader> */}
                                    <ProgresLineaire />
                                </CardContent>
                            </Card>

                            <Grid item md={5} >
                                <Card sx={{ maxWidth: 400, boxShadow: 2 }}>
                                    <CardContent>

                                        <Typography
                                            sx={{
                                                color: 'black',
                                                // border: '1px solid',
                                                background: '#20DF7F',
                                                // minHeight:20,
                                                borderRadius: '20px 20px 0 0',
                                                p: 1,
                                                width: '40vh',
                                                m: '0 auto',
                                                boxShadow: 5

                                            }}
                                            align='left '
                                        >
                                            Statistiques</Typography>
                                        {/* </CardHeader> */}
                                        <ChartLineaire />
                                    </CardContent>
                                </Card>

                            </Grid>

                        </Grid>

                        <Grid item md={10} columnGap={2} sx={{ m: '0px auto', display: 'flex' }} >



                            <Card sx={{ minWidth: 580, ml: '60px', boxShadow: 2 }}>
                                <CardContent>
                                    <AdminTable row={rows} column={columns} />
                                </CardContent>
                            </Card>

                            <Grid item md={5} >
                                <Card sx={{ maxWidth: 450, boxShadow: 2 }}>
                                    <CardContent>
                                        <AdminTable row={rows_2} column={columns_2} />
                                    </CardContent>
                                </Card>

                            </Grid>


                        </Grid>

                        <Grid item md={12} mt={2}></Grid>



                    </Grid>
                ) : (
                    <>
                        <Grid item md={2} direction='row' mb={8}>

                        </Grid>
                        <Typography sx={{ ml: '10px', maxWidth: '30vh' }} justifySelf>
                            <Button
                                variant='contained'
                                sx={{
                                    background: '#20df7f',
                                    maxWidth: 55,
                                    px: 2,
                                    fontSize: 10,
                                    color: 'black',
                                    fontFamily: 'Montserrat',
                                    ':hover': {
                                        background: ''
                                    }
                                }}>
                                Ajouter


                            </Button>
                            &nbsp;
                            <Button
                                sx={{
                                    background: '#e6e6e7',
                                    fontFamily: 'Montserrat',
                                    color: '#20df7f',
                                    border: '1px solid transparent',
                                    maxHeight: 25
                                }}
                                title='Archives'
                                // aria-controls={open ? 'basic-menu' : undefined}
                                // aria-haspopup="true"
                                // aria-expanded={open ? 'true' : undefined}
                                onClick={handleClick}
                            >
                                <LinearScaleIcon />

                            </Button>
                            <Menu
                                id="basic-menu"
                                anchorEl={anchorEl}
                                open={openMenu}
                                onClick={handleClose}
                                MenuListProps={{
                                    'aria-labelledby': 'basic-button',
                                }}
                            >
                                <MenuItem onClick={handleClose}>Archives</MenuItem>
                                <MenuItem onClick={handleClose}>Utilisateurs bloqués</MenuItem>
                                <MenuItem onClick={handleClose}>Cotisation términé</MenuItem>
                            </Menu>
                        </Typography>

                        <Grid
                            item md={8}
                            sx={{
                                // border:'1px solid',
                                display: 'block',
                                m: '0px auto',

                                // m:'0 70px'
                            }}
                            columnGap={2}
                        >

                            <Card sx={{ maxWidth: 200, boxShadow: 8, mb: 1 }}>
                                <CardActionArea>
                                    <CardContent sx={{ textAlign: 'left' }}>
                                        <Typography variant="body2" sx={{ fontFamily: 'Montserrat' }} color="textSecondary" component="p">
                                            Membres Actif

                                        </Typography>
                                        <Typography gutterBottom variant="h5" component="h2" sx={{ fontFamily: 'Montserrat' }}>
                                            94 Membres
                                        </Typography>

                                    </CardContent>
                                </CardActionArea>
                            </Card>
                            <Card sx={{ maxWidth: 200, boxShadow: 8, mb: 1 }}>
                                <CardActionArea>
                                    <CardContent sx={{ textAlign: 'left' }}>
                                        <Typography variant="body2" sx={{ fontFamily: 'Montserrat' }} color="textSecondary" component="p">
                                            Membres Bloqués

                                        </Typography>
                                        <Typography gutterBottom sx={{ fontFamily: 'Montserrat' }} variant="h5" component="h2" >
                                            6 Membres
                                        </Typography>

                                    </CardContent>
                                </CardActionArea>
                            </Card>

                            <Card sx={{ maxWidth: 200, boxShadow: 8, mb: 1 }}>
                                <CardActionArea>
                                    <CardContent sx={{ textAlign: 'left' }}>
                                        <Typography variant="body2" sx={{ fontFamily: 'Montserrat' }} color="textSecondary" component="p">
                                            Total Effectif

                                        </Typography>
                                        <Typography gutterBottom sx={{ fontFamily: 'Montserrat' }} variant="h5" component="h2" >
                                            100 Membres
                                        </Typography>

                                    </CardContent>
                                </CardActionArea>
                            </Card>

                        </Grid>

                        <Grid item md={7} sx={{ m: '0px 40px', justifyContent: 'center' }}>
                            {/* <DataGridDemo/> */}
                            {/* <DataGrid
                                rows={rows}
                                columns={columns}
                                pageSize={4}
                                rowsPerPageOptions={[5]}
                                checkboxSelection
                                disableSelectionOnClick
                                sx={{height:350}}
                            /> */}
                            {/* <ProgresLineaire/> */}
                        </Grid>
                    </>
                )
            }
          
        </Grid>
    )
}



export default AdminDashboard