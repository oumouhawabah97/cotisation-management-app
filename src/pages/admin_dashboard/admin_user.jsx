import React from 'react'
import { Grid, Card, CardContent, Typography, Button, CardActionArea, useMediaQuery } from '@mui/material'
import Menubutton from '../../components/menu_dashboard/menu_button';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';
import BlockIcon from '@mui/icons-material/Block';
import client from "../../services/clientService"
import DataGridDemo from '../../components/table/dataGrid';

function tokenHeader() {
    const user = JSON.parse(localStorage.getItem("user"))

    if (user && user.accessToken) {
        return { "Authorization": "Bearer " + user.accessToken }
    }
}


const AdminUser = () => {
  
    const matches = useMediaQuery('(min-width:980px)');
    // const [open, setOpen] = React.useState(false);
    // const [anchorEl, setAnchorEl] = React.useState(null);
    // const openMenu = Boolean(anchorEl);
    const [users, setUsers] = React.useState('');

    let blockedUsers;
    let activeUsers;
    if (users !== null && typeof users !== "string") {

        blockedUsers = users.filter(user => user.blocked === true)
        activeUsers = users.filter(user => user.blocked === false && user.archiver===false)
    };
    
    // const handleClick = (event) => {
    //   setAnchorEl(event.currentTarget);
    // };
    // const handleClose = () => {
    //   setAnchorEl(null);
    // };
  
    const getUsers = async () => {
        const response = await client.get("/contributions/list/populate", { headers: tokenHeader() })
        const { data } = await response
        setUsers(data)
    }
    
    
    React.useEffect(() => {
        getUsers()
    }, [])
  
    
  
  
  
      
  
    return (
      <Grid 
          container md={12} sm={8} spacing={2} p={3}
              sx={{
                  border:'1px solid', 
                  // m:'50px auto',
                  display:'block',
                  maxHeight:50
                  
              }}
      >
              {
                  matches ?(
                      <>
                              <Grid item md={2} direction='row' mb={6}>
  
                              </Grid>
                            
                              <Grid 
                                  item md={8} 
                                  sx={{
                                      // border:'1px solid',
                                      display:{sm:'block', md:'flex'},
                                      m:'0px auto', 
                                      justifyContent:'center',
                                      
                                  }} 
                                  columnGap={3}
                                  
                                  >
  
                                  <Card sx={{maxWidth:200, boxShadow:4}}>
                                      <CardActionArea>
                                          <CardContent sx={{textAlign:'left'}}>
                                          <Typography variant="body2" sx={{fontFamily:'Montserrat'}} color="textSecondary" component="p">
                                              Membres Actif <FiberManualRecordIcon fontSize='small' color='success'/>
                                              
                                          </Typography>
                                          <Typography gutterBottom variant="h5" component="h2" sx={{fontFamily:'Montserrat'}}>
                                          {(users && typeof users !== "string") ? activeUsers.length : "0"} Membres     
                                          </Typography>
                                          
                                          </CardContent>
                                      </CardActionArea>
                                  </Card>
  
                                  <Card sx={{maxWidth:200, boxShadow:4}}>
                                      <CardActionArea>
                                          <CardContent sx={{textAlign:'left'}}>
                                              <Typography variant="body2" sx={{fontFamily:'Montserrat'}} color="textSecondary" component="p">
                                                  Membres Bloqués <BlockIcon fontSize='small' color='error'/>
                                                  
                                              </Typography>
                                              <Typography gutterBottom sx={{fontFamily:'Montserrat'}} variant="h5" component="h2" >
                                              {(users && typeof users !== "string") ? blockedUsers.length : "0"} Membres     
                                              </Typography>
                                          
                                          </CardContent>
                                      </CardActionArea>
                                  </Card>
  
                                  <Card sx={{maxWidth:200, boxShadow:4}}>
                                      <CardActionArea>
                                          <CardContent sx={{textAlign:'left'}}>
                                          <Typography variant="body2" sx={{fontFamily:'Montserrat'}} color="textSecondary" component="p">
                                              Total Effectif
                                              
                                          </Typography>
                                          <Typography gutterBottom sx={{fontFamily:'Montserrat'}} variant="h5" component="h2" >
                                          {(users && typeof users !== "string") ? users.length : "0"} Membres     
                                          </Typography>
                                          
                                          </CardContent>
                                      </CardActionArea>
                                  </Card>
  
                              </Grid>
  
                              <Typography    sx={{ml:'58rem', maxWidth:'30vh'}} justifySelf>   
                                      <Menubutton />     
                              </Typography>
                              <Grid item md={7} sx={{m:'0px auto',  justifyContent:'center'}}>
                                  <DataGridDemo users={users}/>
                                  {/* <DataGrid
                                      rows={rows}
                                      columns={columns}
                                      pageSize={4}
                                      rowsPerPageOptions={[5]}
                                      checkboxSelection
                                      disableSelectionOnClick
                                      sx={{height:350}}
                                  /> */}
                              </Grid>
                              
                      </>        
                  ):(
                      <> 
                          <Grid item md={2} direction='row' mb={8}>
  
                          </Grid>
                          <Typography    sx={{ml:'10px', maxWidth:'30vh'}} justifySelf>
                              <Button 
                                  variant='contained' 
                                  sx={{
                                      background:'#20df7f', 
                                      maxWidth:55, 
                                      px:2,
                                      fontSize:10,
                                      color:'black',
                                      fontFamily:'Montserrat',    
                                      ':hover':{
                                          background:''
                                      }
                                  }}>
                                  Ajouter
  
                                  
                              </Button>
                              {/* &nbsp;
                              <Button
                                      sx={{
                                          background:'#e6e6e7',  
                                          fontFamily:'Montserrat',    
                                          color:'#20df7f',
                                          border:'1px solid transparent',
                                          maxHeight:25 
                                      }}
                                      title='Archives'
                                      aria-controls={open ? 'basic-menu' : undefined}
                                      aria-haspopup="true"
                                      aria-expanded={open ? 'true' : undefined}
                                      onClick={handleClick}
                                  >
                                     <LinearScaleIcon />
                              
                              </Button>
                                  <Menu
                                      id="basic-menu"
                                      anchorEl={anchorEl}
                                      open={openMenu}
                                      onClick={handleClose}
                                      MenuListProps={{
                                      'aria-labelledby': 'basic-button',
                                      }}
                                  >
                                      <MenuItem onClick={handleClose}>Archives</MenuItem>
                                      <MenuItem onClick={handleClose}>Utilisateurs bloqués</MenuItem>
                                      <MenuItem onClick={handleClose}>Cotisation términé</MenuItem>
                                  </Menu> */}
                          </Typography>
  
                          <Grid 
                              item md={8} 
                              sx={{
                                  // border:'1px solid',
                                  display:'block',
                                //   m:'0px auto', 
                                  
                                  m:'0 70px'
                              }} 
                              columnGap={2}
                              >
  
                              <Card sx={{maxWidth:200, boxShadow:8, mb:1}}>
                                  <CardActionArea>
                                      <CardContent sx={{textAlign:'left'}}>
                                      <Typography variant="body2" sx={{fontFamily:'Montserrat'}} color="textSecondary" component="p">
                                          Membres Actif
                                          
                                      </Typography>
                                      <Typography gutterBottom variant="h5" component="h2" sx={{fontFamily:'Montserrat'}}>
                                          94 Membres     
                                      </Typography>
                                      
                                      </CardContent>
                                  </CardActionArea>
                              </Card>
                              <Card sx={{maxWidth:200, boxShadow:8, mb:1}}>
                                  <CardActionArea>
                                      <CardContent sx={{textAlign:'left'}}>
                                          <Typography variant="body2" sx={{fontFamily:'Montserrat'}} color="textSecondary" component="p">
                                              Membres Bloqués
                                              
                                          </Typography>
                                          <Typography gutterBottom sx={{fontFamily:'Montserrat'}} variant="h5" component="h2" >
                                              6 Membres     
                                          </Typography>
                                      
                                      </CardContent>
                                  </CardActionArea>
                              </Card>
  
                              <Card sx={{maxWidth:200, boxShadow:8, mb:1}}>
                                  <CardActionArea>
                                      <CardContent sx={{textAlign:'left'}}>
                                      <Typography variant="body2" sx={{fontFamily:'Montserrat'}} color="textSecondary" component="p">
                                          Total Effectif
                                          
                                      </Typography>
                                      <Typography gutterBottom sx={{fontFamily:'Montserrat'}} variant="h5" component="h2" >
                                          100 Membres     
                                      </Typography>
                                      
                                      </CardContent>
                                  </CardActionArea>
                              </Card>
  
                          </Grid>
  
                          <Grid item md={7} sx={{m:'0px 40px',  justifyContent:'center'}}>
                              <DataGridDemo/>
                              {/* <DataGrid
                                  rows={rows}
                                  columns={columns}
                                  pageSize={4}
                                  rowsPerPageOptions={[5]}
                                  checkboxSelection
                                  disableSelectionOnClick
                                  sx={{height:350}}
                              /> */}
                          </Grid>
                      </>
                  )
              }
      </Grid>
    )
}

export default AdminUser