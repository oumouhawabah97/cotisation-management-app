import React, { useState } from 'react'
import { Box, Button, Card, CardContent, CardMedia, Grid, Typography } from '@mui/material'
import group_img from '../images/group_img.png'
import TextField from '@mui/material/TextField';
import PhoneTextField from "mui-phone-textfield";
import useMediaQuery from '@mui/material/useMediaQuery';
import swal from 'sweetalert'

const Forget = () => {

    const matches = useMediaQuery('(min-width:980px)');

    const [
        // telephone, 
        setTelephone] = useState('');
    const [phoneNumber, setPhoneNumber] = useState(); // The PhoneNumber instance.

    const [valueNumber, setValuenumber] = useState(""); // The input value.
    const [country, setCountry] = useState("SN");

   

    const onChangeNumber = ({ formattedValue, phoneNumber }) => {
		setValuenumber(formattedValue);
		setPhoneNumber(phoneNumber);
	};

	const onCountrySelect = ({ country, formattedValue, phoneNumber }) => {
		setValuenumber(formattedValue);
		setCountry(country);
		setPhoneNumber(phoneNumber);
	};

    const mosAlert = (e) => {
        swal({
            title:'Message envoyé',
            text:'Nous avons envoyé un message à votre numéro  avec un lien pour réinitialiser votre mot de passe',
            icon:'success',
            button:'OK',
            fontFamily:"Montserratsemibold"
        })
    }
    // const ForgetPassword = (telephone) => {

    //     client
    //         .post('', {
    //             telephone: telephone
    //         })
    //         .then((response) => {
    //             setTimeout(() => {
    //                 alert(response.data.access)
    //             }, 2000);
    //         }).then((e) => alert(e));

    // }
    return (
        <>

            {
                matches ? (
                    <Grid container md={10} sx={{ m: '5px auto', mt: 4, minHeight: '90vh', boxShadow: 4 }}>
                        <Grid item md={4} sx={{ background: '#fff' }}>
                            <Grid
                                sx={{
                                    border: '1px solid',
                                    background: '#093545',
                                    borderRadius: '0px 60px 60px 0px',
                                    height: '90vh',
                                    width: '55vh',
                                }} >

                                <CardMedia h-300 component="img" image={group_img} alt="Paella dish"
                                    sx={{
                                        borderRaduis: 10,
                                        width: 217,
                                        m: '60px auto',
                                        mt: 15,
                                    }} />
                            </Grid>
                        </Grid>

                        <Card sx={{
                            width: 480,
                            alignItems: 'center',
                            height: 330,
                            m: '120px  auto',
                            borderRaduis: 10,
                            boxShadow: 10
                        }}>
                            <CardContent style={{ margin: 0, padding: 0 }}>
                                <Grid item md={10} sx={{
                                    width: '75vh',
                                    m: '0 auto'
                                }}>&nbsp;&nbsp;
                                    <Typography variant='h4' mb={2} mt={1} style={{ fontFamily: 'Montserrat', fontWeight: 'bold', textAlign: 'initial' }}>Mot de passe oublié</Typography>
                                    <Typography variant='subtitle2' mb={2} mt={2} style={{ fontFamily: 'Montserrat', textAlign: 'initial' }}>Pour réinitialiser votre mot de passe entrez votre e-email
                                        ou voter numéro de téléphone
                                    </Typography>

                                    <Box > &nbsp; &nbsp;
                                        <Grid gridRow='auto' sx={{ justifyContent: 'space-around' }}>
                                            <Grid gridRow='auto'>

                                                <Box component="form"

                                                    sx={{
                                                        '& > :not(style)': { m: 1, width: '34ch', },

                                                    }}
                                                    noValidate
                                                    autoComplete="off"  >

                                                    <TextField mt={1} size='small' type='tel'
                                                        label="N°Téléphone "
                                                        sx={{ fontFamily: 'Montserrat', background: "#093545", color: 'white', borderRadius: '5px', width: 100 }}
                                                        inputProps={{ style: { color: "white" } }}
                                                        onChange={e => setTelephone(e.target.value)}
                                                        InputLabelProps={{ style: { color: 'white', width: '200px' } }} />
                                                </Box>

                                                <Box
                                                    component="form"
                                                    sx={{
                                                        '& > :not(style)': { mt: 3, ml: 31, width: '38ch' },
                                                    }}
                                                    noValidate
                                                    autoComplete="off" >
                                                    <Button onClick={() => mosAlert()}
                                                        style={{ fontWeight: 'Bold' }}
                                                        variant="contained" mt={3}
                                                        sx={{ fontFamily: 'bold', maxWidth: '18vh', height: '70', color: 'black', background: '#20df7f', borderRadius: 1 }}>

                                                        Envoyer

                                                    </Button>
                                                </Box>
                                            </Grid>
                                        </Grid>
                                    </Box>
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                ) : (
                    <Grid container md={8} sx={{ m: '5px auto', mt: 4, minHeight: '90vh'}}>

                        <Card sx={{
                             width: 420, 
                             alignItems: 'center', 
                             height: 300, 
                             m: '150px  auto', 
                             borderRaduis: 10, 
                             boxShadow: 10 
                             }}
                        >
                            <CardContent style={{ margin: 0, padding: 0 }}>
                                <Grid item md={6} sx={{ width: '40vh', m: '0 auto' }}>&nbsp;&nbsp;&nbsp;
                                    <Typography variant='h4' mb={2} mt={1} style={{ fontFamily: 'Montserrat', fontWeight: 'bold', textAlign: 'initial' }}>Mot de passe oublié</Typography>
                                    <Typography variant='subtitle2' mb={2} mt={2} style={{ fontFamily: 'Montserrat' }}>Pour reinitialiser votre mot de passe entrez votre e-email
                                        ou voter numéro de téléphone
                                    </Typography>

                                    <Box > 
                                        {/* <Grid gridRow='auto' sx={{ justifyContent: 'space-around' }}> */}
                                            {/* <Grid > */}

                                                <Box component="form"

                                                    sx={{
                                                        '& > :not(style)': { width: '30ch', }
                                     

                                                    }}
                                                    noValidate
                                                    autoComplete="off"  >


                                                    {/* <TextField mt={1} size='small' type='tel'
                                                        label="N°Téléphone "
                                                        sx={{ fontFamily: 'Montserrat', background: "#093545", color: 'white', borderRadius: '5px' }}
                                                        inputProps={{ style: { color: "white" } }}
                                                        onChange={e => setTelephone(e.target.value)}
                                                        InputLabelProps={{ style: { color: 'white', width: '200px' } }} /> */}
                                                        <PhoneTextField
                                                            label="Phone number"
                                                            error={Boolean(valueNumber && phoneNumber?.country !== country)}
                                                            value={valueNumber}
                                                            country={country}
                                                            onCountrySelect={onCountrySelect}
                                                            onChange={onChangeNumber}
                                                            sx={{ 
                                                                fontFamily:
                                                                 'Montserrat', 
                                                                 color: 'white', 
                                                                 borderRadius: '5px', 
                                                                 width: '25ch', 
                                                            }}
                                                            size="meduim"
                                                            required
                                                        />
                                                </Box>


                                                {/* <Box
                                                    component="form"
                                                    sx={{
                                                        '& > :not(style)': { m:'0 auto', width: '38ch',border:'1px solid blue' },
                                                    }}
                                                    noValidate
                                                    autoComplete="off" > */}
                                                    <Button  onClick={()=> mosAlert()}
                                                        style={{ fontWeight: 'Bold' }}
                                                        variant="contained" 
                                                        sx={{ 
                                                            fontFamily: 'bold',  
                                                            maxWidth: '18vh',
                                                            height:'80', 
                                                            color: 'black', 
                                                            background: '#20df7f', 
                                                            borderRadius: 1 ,
                                                            m:'10px 225px',
                                                            
                                                        }}
                                                    >

                                                        Envoyer

                                                    </Button>
                                                {/* </Box> */}
                                            {/* </Grid> */}
                                        {/* </Grid> */}
                                    </Box>
                                </Grid>
                            </CardContent>
                        </Card>
                    </Grid>
                )

            }

        </>
    )
}

export default Forget
