import { Navigate } from 'react-router-dom'
import RouterOutlet from '../router_outlet/routerOutlet';

const PrivateRoute = () => {
    const ROLE_LIST={
        Admin: 5150,
        User: 2001
        
    }
    const user = JSON.parse(localStorage.getItem('user'))
    // const [isAdmin, setisAdmin] = useState(false)
  
   return (user && user.user.roles) && user.user.roles.includes(ROLE_LIST.Admin)?(
        <RouterOutlet/>
    ):(
        <Navigate to='/'/>
    )
   
//   return   isAdmin? <RouterOutlet/>: <Navigate to='/'/>;
}

export default PrivateRoute