import React, { useState } from 'react'
import { Box, Button, CardMedia, Checkbox, Grid, Typography, useMediaQuery } from '@mui/material'
import login_img from '../images/projet.png'
import TextField from '@mui/material/TextField';
import client from '../services/clientService';
import LoadingButton from '@mui/lab/LoadingButton';
import { Link, useNavigate } from 'react-router-dom';
// import useMediaQuery from '@mui/material/useMediaQuery';
import useAuth from "../hooks/useAuth"
const Login = () => {


    const { auth } = useAuth()

    const [phone, setPhone] = useState('');
    const [password, setPassword] = useState('')
    const [errorMsg, setErrorMsg] = useState('')
    const [
        loader,
        //  setloader
    ] = useState(false)

    const matches = useMediaQuery('(min-width:980px)');

    const navigate = useNavigate()

    const handleSubmit = (e) => {
        e.preventDefault();
        Connexion(phone, password)
    }

    const Connexion = async (phone, password) => {
        const response = await client.post('/auth', { phone, password })
        const { data } = await response;
        console.log(data)
        try {
            if (data.accessToken) {
                await localStorage.setItem("user", JSON.stringify(data))
    
            }
            (auth && auth.user.roles) && auth.user.roles.includes(5150) ? navigate("/admin/dashboard") : navigate("/cotisation")
            return data
            // else{
            //     await setErrorMsg(data.message)
            //     setTimeout(() => {
            //         setErrorMsg('')
            //     }, 2000);
            // }
        } catch (error) {
            console.log(error)
             setErrorMsg(error)
                setTimeout(() => {
                    setErrorMsg('')
                }, 2000);
        }

    }

    return (
        <>
            {
                matches ? (
                    <Grid container item md={8} sx={{ m: '15px auto', minHeight: '90vh', boxShadow: 3 }}>

                        <Grid item md={5} sx={{ background: '#fff' }}>
                            <Grid
                                sx={{
                                    border: '1px solid',
                                    background: '#093545',
                                    borderRadius: '0px 60px 60px 0px',
                                    height: '90vh',
                                    width: '60vh'
                                }}
                            >
                                <CardMedia
                                    component="img"
                                    image={login_img}
                                    alt="Paella dish"
                                    sx={{
                                        borderRaduis: 20,
                                        width: 200,
                                        m: '105px auto',
                                    }}
                                />
                            </Grid>
                        </Grid>

                        <Grid item md={7}  >
                            &nbsp;&nbsp;
                            <Typography
                                variant='h5'
                                sx={{
                                    fontFamily: 'Montserrat'
                                    // , minWidth:'95vh'
                                    , fontWeight: 'bold'
                                }}
                            // mt={4}
                            // mr={2}
                            >
                                Bienvenue sur votre Money-Subscribe
                            </Typography>
                            <Typography variant='subtitle2' sx={{ m: '0 160px' }}>
                                <strong style={{ fontSize: 20, fontFamily: 'Montserrat' }}>Connectez-vous</strong>
                            </Typography>

                            <Box
                                sx={{
                                    width: 320,
                                    height: 400,
                                    m: '0 100px'
                                }}
                            >

                                <Typography variant='subtitle2' mb={2} mt={2} sx={{ fontFamily: 'Montserrat' }}>Connectez-vous et gérez vos cotisations</Typography>

                                <Grid gridRow='auto'>
                                    <Box
                                        component="form"
                                        sx={{
                                            '& > :not(style)': { m: 1, width: '30ch', },

                                        }}
                                        noValidate
                                        autoComplete="off"
                                    >

                                        <TextField
                                            label="N°téléphone ou E-mail"
                                            sx={{ background: "#093545", height: '51px', color: 'white', borderRadius: '10px' }}
                                            inputProps={{ style: { color: "white" } }}
                                            InputLabelProps={{ style: { color: 'white', fontFamily: 'Montserrat' } }}
                                            onChange={e => setPhone(e.target.value)}
                                            variant='filled'
                                        />

                                    </Box>
                                </Grid>
                                <Grid gridRow='auto'>
                                    <Box
                                        component="form"
                                        sx={{
                                            '& > :not(style)': { m: 1, width: '30ch' },
                                        }}
                                        noValidate
                                        autoComplete="off"
                                    >

                                        <TextField
                                            label="Mot de passe"
                                            sx={{ background: "#093545", height: '51px', color: 'white', borderRadius: '10px' }}
                                            inputProps={{ style: { color: "white" } }}
                                            InputLabelProps={{ style: { color: 'white', fontFamily: 'Montserrat ' } }}
                                            type='password'
                                            onChange={e => setPassword(e.target.value)}
                                            variant='filled'
                                        />

                                    </Box>
                                </Grid>
                                <Typography color='error' variant='body1'>{errorMsg}</Typography>
                                <Grid gridRow='auto' mt={3}>
                                    <Box
                                        component="form"
                                        // sx={{
                                        //     '& > :not(style)': { m: 0,},
                                        // }}

                                        noValidate
                                        autoComplete="off"
                                    >
                                        <Typography variant='caption'  >
                                            <Checkbox
                                                color='secondary'
                                                sx={{
                                                    color: "green",
                                                    // background: "#093545",
                                                    '&.Mui-checked': {
                                                        color: "#093545",
                                                    },
                                                    fontFamily: 'Montserrat '
                                                }}
                                                defaultChecked
                                            />
                                            Restez Connecté
                                        </Typography>
                                        &nbsp;&nbsp;
                                        <Typography variant='caption' sx={{ fontFamily: 'Montserrat' }}>
                                            {/* <strong> */}
                                            <Link to="forget"
                                                style={{
                                                    textDecoration: 'none',
                                                    color: 'black',
                                                    fontFamily: 'Montserrat',
                                                    fontWeight: "bold"
                                                }}>
                                                Mot de passe oublié
                                            </Link>
                                            {/* </strong> */}
                                        </Typography>

                                    </Box>

                                    <Box
                                        component="form"
                                        sx={{
                                            '& > :not(style)': { m: 1, width: '29ch' },
                                        }}
                                        noValidate
                                        autoComplete="off"
                                    >
                                        {
                                            !phone || !password ? (
                                                <Button
                                                    variant="contained"
                                                    disabled
                                                    sx={{
                                                        maxWidth: '50vh',
                                                        color: '#093545',
                                                        fontFamily: 'Montserrat',
                                                        background: '#20df7f',
                                                        boxShadow: 8,
                                                        ':hover': {
                                                            bgcolor: '#00fe66',
                                                            color: 'white',
                                                            background: '#093545',

                                                        },
                                                    }}
                                                    onClick={e => handleSubmit(e)}
                                                    style={{ fontWeight: 'Bold' }}
                                                >
                                                    Connexion
                                                    {
                                                        loader && <LoadingButton
                                                            size="small"
                                                            sx={{ maxWidth: '2vh', p: 0, border: 'none' }}
                                                            // onClick={handleClick}
                                                            endIcon={'SendIcon'}
                                                            loading={loader}
                                                            loadingPosition="center"
                                                            variant="outlined"
                                                        >

                                                        </LoadingButton>
                                                    }
                                                </Button>
                                            ) : (
                                                <Button
                                                    variant="contained"
                                                    sx={{
                                                        maxWidth: '50vh',
                                                        color: '#093545',
                                                        fontFamily: 'Montserrat',
                                                        background: '#20df7f',
                                                        boxShadow: 8,
                                                        ':hover': {
                                                            bgcolor: '#00fe66',
                                                            color: 'white',
                                                            background: '#093545',

                                                        },
                                                    }}
                                                    onClick={e => handleSubmit(e)}
                                                    style={{ fontWeight: 'Bold' }}
                                                >
                                                    Connexion
                                                    {
                                                        loader && <LoadingButton
                                                            size="small"
                                                            sx={{ maxWidth: '2vh', p: 0, border: 'none' }}
                                                            // onClick={handleClick}
                                                            endIcon={'SendIcon'}
                                                            loading={loader}
                                                            loadingPosition="center"
                                                            variant="outlined"
                                                        >

                                                        </LoadingButton>
                                                    }
                                                </Button>
                                            )
                                        }
                                    </Box>
                                </Grid>

                            </Box>



                        </Grid>

                    </Grid>
                ) : (
                    <Grid container sx={{ m: '150px auto' }}>

                        <Grid item md={12}  >
                            &nbsp;&nbsp;
                            <Typography
                                variant='h5'
                                sx={{
                                    fontFamily: 'Montserrat'
                                    // , minWidth:'95vh'
                                    , fontWeight: 'bold'
                                }}
                            // mt={4}
                            // mr={2}
                            >
                                Bienvenue sur votre Money-Subscribe
                            </Typography>
                            <Typography variant='subtitle2' >
                                <strong style={{ fontSize: 20, fontFamily: 'Montserrat' }}>Connectez-vous</strong>
                            </Typography>

                            <Box
                                sx={{
                                    // width: 320,
                                    // height: 400,
                                    // m:'0 100px',
                                    // border:'1px solid',


                                }}
                            >

                                <Typography variant='subtitle2' mb={2} mt={2} sx={{ fontFamily: 'Montserrat' }}>Connectez-vous et gérez vos cotisations</Typography>

                                <Grid gridRow='auto'>
                                    <Box
                                        component="form"
                                        sx={{
                                            '& > :not(style)': { m: 1, width: '30ch', },

                                        }}
                                        noValidate
                                        autoComplete="off"
                                    >

                                        <TextField
                                            label="N°téléphone ou E-mail"
                                            sx={{ background: "#093545", height: '51px', color: 'white', borderRadius: '10px' }}
                                            inputProps={{ style: { color: "white" } }}
                                            InputLabelProps={{ style: { color: 'white', fontFamily: 'Montserrat' } }}
                                            onChange={e => setPhone(e.target.value)}
                                            variant='filled'
                                        />

                                    </Box>
                                </Grid>
                                <Grid gridRow='auto'>
                                    <Box
                                        component="form"
                                        sx={{
                                            '& > :not(style)': { m: 1, width: '30ch' },
                                        }}
                                        noValidate
                                        autoComplete="off"
                                    >

                                        <TextField
                                            label="Mot de passe"
                                            sx={{ background: "#093545", height: '51px', color: 'white', borderRadius: '10px' }}
                                            inputProps={{ style: { color: "white" } }}
                                            InputLabelProps={{ style: { color: 'white', fontFamily: 'Montserrat ' } }}
                                            type='password'
                                            onChange={e => setPassword(e.target.value)}
                                            variant='filled'
                                        />

                                    </Box>
                                </Grid>

                                <Grid gridRow='auto' mt={3}>
                                    <Box
                                        component="form"
                                        // sx={{
                                        //     '& > :not(style)': { m: 0,},
                                        // }}

                                        noValidate
                                        autoComplete="off"
                                    >
                                        <Typography variant='caption'  >
                                            <Checkbox
                                                color='secondary'
                                                sx={{
                                                    color: "green",
                                                    // background: "#093545",
                                                    '&.Mui-checked': {
                                                        color: "#093545",
                                                    },
                                                    fontFamily: 'Montserrat '
                                                }}
                                                defaultChecked
                                            />
                                            Restez Connecté
                                        </Typography>
                                        &nbsp;&nbsp;
                                        <Typography variant='caption' sx={{ fontFamily: 'Montserrat' }}>
                                            {/* <strong> */}
                                            <Link to="forget"
                                                style={{
                                                    textDecoration: 'none',
                                                    color: 'black',
                                                    fontFamily: 'Montserrat',
                                                    fontWeight: "bold"
                                                }}>
                                                Mot de passe oublié
                                            </Link>
                                            {/* </strong> */}
                                        </Typography>

                                    </Box>

                                    <Box
                                        component="form"
                                        sx={{
                                            '& > :not(style)': { m: 1, width: '29ch' },
                                        }}
                                        noValidate
                                        autoComplete="off"
                                    >
                                        {
                                            !phone || !password ? (
                                                <Button
                                                    variant="contained"
                                                    disabled
                                                    sx={{
                                                        maxWidth: '50vh',
                                                        color: '#093545',
                                                        fontFamily: 'Montserrat',
                                                        background: '#20df7f',
                                                        boxShadow: 8,
                                                        ':hover': {
                                                            bgcolor: '#00fe66',
                                                            color: 'white',
                                                            background: '#093545',

                                                        },
                                                    }}
                                                    onClick={e => handleSubmit(e)}
                                                    style={{ fontWeight: 'Bold' }}
                                                >
                                                    Connexion
                                                    {
                                                        loader && <LoadingButton
                                                            size="small"
                                                            sx={{ maxWidth: '2vh', p: 0, border: 'none' }}
                                                            // onClick={handleClick}
                                                            endIcon={'SendIcon'}
                                                            loading={loader}
                                                            loadingPosition="center"
                                                            variant="outlined"
                                                        >

                                                        </LoadingButton>
                                                    }
                                                </Button>
                                            ) : (
                                                <Button
                                                    variant="contained"
                                                    sx={{
                                                        maxWidth: '50vh',
                                                        color: '#093545',
                                                        fontFamily: 'Montserrat',
                                                        background: '#20df7f',
                                                        boxShadow: 8,
                                                        ':hover': {
                                                            bgcolor: '#00fe66',
                                                            color: 'white',
                                                            background: '#093545',

                                                        },
                                                    }}
                                                    onClick={e => handleSubmit(e)}
                                                    style={{ fontWeight: 'Bold' }}
                                                >
                                                    Connexion
                                                    {
                                                        loader && <LoadingButton
                                                            size="small"
                                                            sx={{ maxWidth: '2vh', p: 0, border: 'none' }}
                                                            // onClick={handleClick}
                                                            endIcon={'SendIcon'}
                                                            loading={loader}
                                                            loadingPosition="center"
                                                            variant="outlined"
                                                        >

                                                        </LoadingButton>
                                                    }
                                                </Button>
                                            )
                                        }
                                    </Box>
                                </Grid>
                                &nbsp;
                                <Typography variant='body2' sx={{ fontFamily: 'Montserrat ' }}  >
                                    Vous n'avez pas de compte,

                                    <Link to="signup"
                                        style={{
                                            textDecoration: 'none',
                                            color: 'black',
                                            fontFamily: 'Montserrat',
                                            fontWeight: "bold"
                                        }}>
                                        Inscrivez-vous
                                    </Link>
                                </Typography>
                            </Box>



                        </Grid>

                    </Grid>
                )
            }
        </>
    )
}

export default Login