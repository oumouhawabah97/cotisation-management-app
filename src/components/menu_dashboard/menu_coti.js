import React from 'react'
import { Button, Menu, MenuItem, Typography } from '@mui/material'
import LinearScaleIcon from '@mui/icons-material/LinearScale';

const Menu_coti = () => {

    const [open] = React.useState(false);
    const [anchorEl, setAnchorEl] = React.useState(null);
    const openMenu = Boolean(anchorEl);
    
    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
    const handleClose = () => {
      setAnchorEl(null);
    };
  return (
    <>
        <Typography    sx={{ml:'10px', maxWidth:'30vh'}} justifySelf>
                           
                           
                            <Button
                                    sx={{
                                        background:'#e6e6e7',  
                                        fontFamily:'Montserrat',    
                                        color:'#20df7f',
                                        border:'1px solid transparent',
                                        maxHeight:25 
                                    }}
                                    title='Archives'
                                    aria-controls={open ? 'basic-menu' : undefined}
                                    aria-haspopup="true"
                                    aria-expanded={open ? 'true' : undefined}
                                    onClick={handleClick}
                                >
                                   <LinearScaleIcon />
                            
                            </Button>
                                <Menu
                                    id="basic-menu"
                                    anchorEl={anchorEl}
                                    open={openMenu}
                                    onClick={handleClose}
                                    MenuListProps={{
                                    'aria-labelledby': 'basic-button',
                                    }}
                                >
                                    <MenuItem onClick={handleClose}>Archives</MenuItem>
                                    <MenuItem onClick={handleClose}>Utilisateurs bloqués</MenuItem>
                                    <MenuItem onClick={handleClose}>Cotisation términé</MenuItem>
                                </Menu>
                        </Typography>
    </>
  )
}

export default Menu_coti