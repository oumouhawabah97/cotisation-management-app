import React, { useState } from 'react'
import { Avatar, Button, Stack, Typography } from '@mui/material'
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import useMediaQuery from '@mui/material/useMediaQuery';
// import { useTheme } from '@mui/material/styles';
import {Box, Grid} from '@mui/material'
import TextField from '@mui/material/TextField';
import { Link } from 'react-router-dom';
import PhoneTextField from "mui-phone-textfield";
import client from "../../services/clientService"
import { ToastContainer, toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';


const SignIn = () => {
    
    const matches = useMediaQuery('(min-width:900px)');
    const [date, setDate] = useState('')
    const [email, setEmail] = useState('')
    const [prenom, setPrenom] = useState('')
    const [montant, setMontant] = useState('')
    const [adresse, setAdresse] = useState('')
    const [profession, setProfession] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirpassword] = useState('')
    const [organisation, setOrganisation] = useState('')
	const [phoneNumber, setPhoneNumber] = useState(); // The PhoneNumber instance.

    const [valueNumber, setValuenumber] = useState(""); // The input value.
    const [country, setCountry] = useState("SN"); // The selected country.

    const [step, setStep] = useState(false)
    const [msgFailed, setMsgfailed] = useState(false)



    const onChangeNumber = ({ formattedValue, phoneNumber }) => {
		setValuenumber(formattedValue);
		setPhoneNumber(phoneNumber);
	};

	const onCountrySelect = ({ country, formattedValue, phoneNumber }) => {
		setValuenumber(formattedValue);
		setCountry(country);
		setPhoneNumber(phoneNumber);
	};


    const handleForm= ()=>{
        setStep(true)
    }

    const handleForm2 = async () => {
        try {
            const response = await client.post('/register', {
                firstname: prenom,
                lastname:  email, phone: parseInt(phoneNumber.number), password, birthday: date,
                address: adresse,
                organisation,
                profession
            })
            const { data } = await response;
            console.log(data)
        } catch (err) {
            console.log(err.message)
        }
    }

    const checkPassword =()=>{
        if(password !== confirmPassword ){
            setStep(false)
            setMsgfailed(true);
            setPassword('');
            setConfirpassword('') 
            return false
        }else{
            handleForm2()
        }
    }
    
    const cotise=async()=>{
        const user= JSON.parse(localStorage.getItem("user"))
        const token = user.accessToken
        // alert('ok')
        // return;

        client.post("contributions",
        {
            amount: montant,
            date: date
        },
        
        { headers: {"Authorization" : `Bearer ${token}`} })
        .then((res)=>{
            console.log(res)
            if(res.data.message){
                toast.success(res.data.message, {
                      position: toast.POSITION.BOTTOM_CENTER
                });
                
                setTimeout(() => {
                    window.location.reload()
                }, 2000);
            }
        });

        
    }
    

  return (
    <>
        {
            matches?(
                <Grid 
                    container 
                    spacing={2} 
                    sx={{ pt:4, boxShadow:2, fontFamily:'Montserrat'}}
                >

                
                   
                    {/* <Grid item md={12}  >
                            <TextField 
                                    type='date' 
                                    label="Date de naissance" 
                                    value={date}
                                    onChange={e=> setDate(e.target.value)}
                                    variant="outlined" 
                                    sx={{mr:12, borderRadius:5, width: '25ch'}}
                                    size="small"
                                    inputProps={{InputProps: {min: "2022-04-17", max: "2023-05-04"} }}
                                    InputLabelProps={{ shrink: true, required: true }}
                                    />
                    </Grid>
                
                   
                    <Grid item md={12} sx={{m:'0 auto'}}>
                                <TextField 
                                    
                                    label="Montant" 
                                    value={montant}
                                    onChange={e=> setMontant(e.target.value)}
                                    variant="outlined" 
                                    sx={{ borderRadius:5, mr:12, width: '25ch'}}
                                    size="small"
                                    InputLabelProps={{ shrink: true, required: true }}
                                />
                    </Grid> */}
                    

                         <Stack
                        component="form"
                        sx={{
                            width: '25ch',
                            m:'0 auto'
                        }}
                        spacing={2}
                        noValidate
                        autoComplete="off"
                        >
                        <TextField 
                                    type='date' 
                                    label="Date de naissance" 
                                    value={date}
                                    onChange={e=> setDate(e.target.value)}
                                    variant="outlined" 
                                    sx={{mr:12, borderRadius:5, width: '25ch'}}
                                    size="small"
                                    inputProps={{InputProps: {min: "2022-04-17", max: "2023-05-04"} }}
                                    InputLabelProps={{ shrink: true, required: true }}
                                    />
                        <TextField 
                                    
                                    label="Montant" 
                                    value={montant}
                                    onChange={e=> setMontant(e.target.value)}
                                    variant="outlined" 
                                    sx={{ borderRadius:5, mr:12, width: '25ch'}}
                                    size="small"
                                    InputLabelProps={{ shrink: true, required: true }}
                                />
                        </Stack>

                    <Box
                            component="form"
                            sx={{
                                '& > :not(style)': { width: '38ch' },
                            }}
                            noValidate
                            autoComplete="off"
                            style={{maxWidth:'36ch',margin:'15px auto'}}
                            >
                            

                            {
                                 
                                        ( !date || !montant)?(
                                          

                                                <Button 
                                                variant="contained"  
                                                onClick={()=> handleForm()} 
                                                disabled
                                                sx={{
                                                    maxWidth:'50vh', 
                                                    fontFamily:'Montserrat', 
                                                    background:'#20df7f',
                                                    boxShadow:8,
                                                    ':hover': {
                                                        bgcolor: '#00fe66', // theme.palette.primary.main
                                                        color: 'white',
                                                        background:'#093545', 

                                                        },
                                                }}
                                                >
                                                Cotiser
                                                </Button>
                                        ):(
                                            <Button 
                                            variant="contained" 
                                            onClick={()=> {cotise()}} 
                                            sx={{
                                                maxWidth:'50vh', 
                                                fontFamily:'Montserrat', 
                                                background:'#20df7f',
                                                boxShadow:8,
                                                ':hover': {
                                                    bgcolor: '#00fe66', // theme.palette.primary.main
                                                    color: 'white',
                                                    background:'#093545', 

                                                    },
                                            }}
                                    
                                        >
                                            Cotiser
                                            </Button>
                                        ) 
                                    
                                }

                    </Box>
                </Grid>
            ):(
                <Grid container  >

                   
                    <Grid item md={6} sx={{m:'100px auto'}}>

                            <Box
                                // component="form"
                                sx={{
                                    '& .MuiTextField-root': { m: 1 },
                                    m:'80px auto',
                                    // background:'#093545'
                                }}
                                noValidate
                                autoComplete="off"
                            >
                                <Typography variant='h5'  align='center' mt={4} sx={{color:'black'}}>INSCRIVEZ-VOUS</Typography>
                               

                                        {
                                            !step?(
                                                <>
                                                <div style={{flexDirection:'column', justifyContent: 'center',}}>

                                                    <TextField 
                                                        
                                                        label="Nom" 
                                                        value={montant}
                                                        onChange={e=> setMontant(e.target.value)}
                                                        type='text'
                                                        sx={{ color:'white', borderRadius:5, width: '25ch'}}
                                                        inputProps={{ style: { color: "black" } }}
                                                        size="small" 
                                                        InputLabelProps={{ shrink: true, required: true }}
                                                        color="secondary"
                                                    />
                                                </div>
                                          
                                                <div style={{flexDirection:'column'}}>

                                                    <TextField 
                                                        label="Prénom" 
                                                        value={prenom}
                                                        onChange={e=> setPrenom(e.target.value)}
                                                        type='text'
                                                        sx={{ color:'white', borderRadius:5, width: '25ch'}}
                                                        inputProps={{ style: { color: "black" } }}
                                                        size="small" 
                                                        InputLabelProps={{ shrink: true, required: true }}
                                                        color="secondary"
                                                    />
                                                </div>
                                            
                                                <div style={{flexDirection:'column'}}>

                                                    <TextField 
                                                        type='date' 
                                                        label="Date de naissance" 
                                                        value={date}
                                                        onChange={e=> setDate(e.target.value)}
                                                        variant="outlined" 
                                                        sx={{ color:'white', borderRadius:5, width: '25ch'}}
                                                        inputProps={{ style: { color: "black" } }}
                                                        size="small" 
                                                        InputLabelProps={{ shrink: true, required: true }}
                                                        color="secondary"
                                                    />
                                                </div>
                                                
                                                
                                                <div style={{flexDirection:'column'}}>

                                                    <TextField 
                                                        type='password'
                                                        label="Mot de passe" 
                                                        value={password}
                                                        onChange={e=> setPassword(e.target.value)}
                                                        sx={{ color:'white', borderRadius:5, width: '25ch'}}
                                                        inputProps={{ style: { color: "black" } }}
                                                        size="small" 
                                                        InputLabelProps={{ shrink: true, required: true }}
                                                        color="secondary"
                                                    />
                                                </div>

                                                
                                                
                                                <div style={{flexDirection:'column'}}>

                                                    <TextField
                                                        type='password' 
                                                        label="Confirme mot de passe"
                                                        value={confirmPassword}
                                                        onChange={e=> setConfirpassword(e.target.value)} 
                                                        sx={{ color:'white', borderRadius:5, width: '25ch'}}
                                                        inputProps={{ style: { color: "black" } }}
                                                        size="small" 
                                                        InputLabelProps={{ shrink: true, required: true }}
                                                        color="secondary"
                                                        error={msgFailed} 
                                                    />

                                                </div>

                                                
                                                

                                                
                                                </>
                                            ):(
                                                
                                                <>
                                               
                                                <div style={{flexDirection:'column'}}>

                                                    <TextField 
                                                        type='email'
                                                        label="Email" 
                                                        value={email}
                                                        onChange={e=> setEmail(e.target.value)}
                                                        sx={{ color:'white', borderRadius:5, width: '25ch'}}
                                                        inputProps={{ style: { color: "black" } }}
                                                        size="small" 
                                                        InputLabelProps={{ shrink: true, required: true }}
                                                        color="secondary"
                                                    />
                                                </div>

                                            

                                            <div style={{flexDirection:'column'}}>
                                                <PhoneTextField
                                                    label="Phone number"
                                                    error={Boolean(valueNumber && phoneNumber?.country !== country)}
                                                    value={valueNumber}
                                                    country={country}
                                                    onCountrySelect={onCountrySelect}
                                                    onChange={onChangeNumber}
                                                    sx={{ color:'white', borderRadius:5, width: '25ch'}}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small" 
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                    required
                                                />
                                            </div>
                                            
                                            
                                            <div style={{flexDirection:'column'}}>
                                                <TextField 
                                                    label="Profession" 
                                                    type='text'
                                                    value={profession}
                                                    onChange={e=> setProfession(e.target.value)}
                                                    sx={{ color:'white', borderRadius:5, width: '25ch'}}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small" 
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                    required
                                                />
                                            </div>
                                        
                                        <div style={{flexDirection:'column'}}>
                                                <TextField 
                                                    type='text' 
                                                    label="Adresse" 
                                                    value={adresse}
                                                    onChange={e=> setAdresse(e.target.value)}
                                                    sx={{ color:'white', borderRadius:5, width: '25ch'}}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small" 
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                    required
                                                />
                                            </div>
                                        
                                        <div style={{flexDirection:'column'}}>
                                                <TextField 
                                                    label="Organisation" 
                                                    type='text'
                                                    value={organisation}
                                                    onChange={e=> setOrganisation(e.target.value)}
                                                    sx={{ color:'white', borderRadius:5, width: '25ch'}}
                                                    inputProps={{ style: { color: "black" } }}
                                                    size="small" 
                                                    InputLabelProps={{ shrink: true, required: true }}
                                                    color="secondary"
                                                    required
                                                />
                                            </div>
                                        

                                                
                                                </>
                                            )
                                            
                                        }            
                                    
                                        {
                                            // msgFailed && <Typography variant='body2' mt color='error'>error</Typography>
                                        }
                                    {/* </div> */}

                                    <Box
                                        // component="form"
                                        sx={{
                                            '& > :not(style)': { width: '25ch' },
                                        }}
                                        noValidate
                                        autoComplete="off"
                                        style={{margin:'15px auto'}}
                                        >
                                    
                                        {
                                            step?(
                                                <Button 
                                                    variant="contained"  
                                                    sx={{
                                                        maxWidth:'50vh', 
                                                        fontFamily:'Montserrat', 
                                                        background:'#20df7f',
                                                        boxShadow:8,
                                                        m:'0 40px',
                                                        ':hover': {
                                                            bgcolor: '#00fe66', // theme.palette.primary.main
                                                            color: 'white',
                                                            background:'#093545', 
                    
                                                        },
                                                    }}
                                                >
                                                    Connexion
                                                </Button>
                                            ):(
                                                ( !prenom || !date || !password || !confirmPassword)?(
                                                    <Button 
                                                        variant="contained" 
                                                        onClick={()=> handleForm()} 
                                                        disabled
                                                        sx={{
                                                            maxWidth:'50vh', 
                                                            fontFamily:'Montserrat', 
                                                            background:'#fff',
                                                            boxShadow:8,
                                                            border:'2px white solid',
                                                            m:'0 40px',
                                                            ':hover': {
                                                                bgcolor: '#00fe66', // theme.palette.primary.main
                                                                color: 'white',
                                                                background:'#093545', 
                        
                                                            },
                                                        }}
                                                
                                                    >
                                                        NEXT
                                                    </Button>
                                                ):(
                                                    <Button 
                                                    variant="contained" 
                                                    onClick={()=> {checkPassword()}} 
                                                    sx={{
                                                        maxWidth:'50vh', 
                                                        fontFamily:'Montserrat', 
                                                        background:'#20df7f',
                                                        boxShadow:8,
                                                        m:'0 40px',
                                                        ':hover': {
                                                            bgcolor: '#00fe66', // theme.palette.primary.main
                                                            color: 'white',
                                                            background:'#093545', 
                    
                                                        },
                                                    }}
                                            
                                                >
                                                    NEXT
                                                </Button>
                                                ) 
                                            )
                                        }
                                        &nbsp;
                                         <Typography variant='body2'sx={{fontFamily:'Montserrat', color:"black",  m:'10px auto'}}  >
                                        Vous n'avez pas de compte, 
                                        <strong>
                                            <Link to="/" style={{textDecoration:'none', color:'#093545',fontWeight:'bold'}}>Connectez-vous</Link>
                                        </strong>
                                    </Typography>
                                    </Box>
                                    &nbsp;
                                   
                            </Box>

                    </Grid>
                    



                </Grid>
            )
        }
        
    </>
  )
}

 function ResponsiveDialog({boolean}) {
  const [open, setOpen] = React.useState(false);
//   const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
   

<div>
    <Button 
        variant='contained' 
        onClick={handleClickOpen}
        sx={{
            background:'#093545', 
            minWidth:20 , 
            px:2,
            fontSize:10,
            color:'white',
            fontFamily:'Montserrat',    
            ':hover':{
                background:'#0f3545'
            }
        }}>
        Je cotise

        
    </Button>
    <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        sx={{border:'1px solid'}}
    >
    <DialogTitle id="alert-dialog-title">
        {/* {"Use Google's location service?"} */}
        <Typography variant='h5' align='center' sx={{height:50, maxWidth:50, m:'0 auto'}} > 
                <Avatar src='im' sizes='meduim'/>
        </Typography>
        <Typography variant='h5' align='center' sx={{fontFamily:'Montserrat', fontWeight:'bold'}} > 
            Nom prenom
        </Typography>
        <Typography variant='body1' align='center' sx={{fontFamily:'Montserrat'}} > 
            Ajouter une cotisation
        </Typography>
    </DialogTitle>
    <DialogContent>
        <DialogContentText id="alert-dialog-description">
        {SignIn()}
        </DialogContentText>
    </DialogContent>
    </Dialog>

</div>
  );
}


const MenuCotisation = () => {

  return (
    <>
        <Typography    sx={{ml:'10px', maxWidth:'30vh', display:'flex'}} justifySelf>
                
                <ResponsiveDialog />
                &nbsp;
                {/* <Button
                        sx={{
                            background:'#e6e6e7',  
                            fontFamily:'Montserrat',    
                            color:'#20df7f',
                            border:'1px solid transparent',
                            maxHeight:25 
                        }}
                        title='Archives'
                        aria-controls={open ? 'basic-menu' : undefined}
                        aria-haspopup="true"
                        aria-expanded={open ? 'true' : undefined}
                        onClick={handleClick}
                    >
                        <LinearScaleIcon />
                
                </Button>
                    <Menu
                        id="basic-menu"
                        anchorEl={anchorEl}
                        open={openMenu}
                        onClick={handleClose}
                        MenuListProps={{
                        'aria-labelledby': 'basic-button',
                        }}
                    >
                        <MenuItem onClick={handleClose}>Archives</MenuItem>
                        <MenuItem onClick={handleClose}>Utilisateurs bloqués</MenuItem>
                        <MenuItem onClick={handleClose}>Cotisation términé</MenuItem>
                    </Menu> */}
        </Typography>

        <ToastContainer />
    </>
  )
}

export default MenuCotisation