import React, { useState } from 'react';
import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import MuiDrawer from '@mui/material/Drawer';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import useAuth from "../hooks/useAuth"

import SettingsIcon from '@mui/icons-material/Settings';
import MonetizationOnIcon from '@mui/icons-material/MonetizationOn';
import PersonRoundedIcon from '@mui/icons-material/PersonRounded';
import GridViewRoundedIcon from '@mui/icons-material/GridViewRounded';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import { Avatar, Menu } from '@mui/material';
import MenuItem from '@mui/material/MenuItem';
import { useNavigate } from 'react-router-dom';
import { ExpandLess, ExpandMore, } from '@mui/icons-material';


const drawerWidth = 240;

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: 'hidden',
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: 'hidden',
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up('sm')]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));



const AppBar = styled(MuiAppBar, {

  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    width: drawerWidth,
    flexShrink: 0,
    background: '#093545',
    whiteSpace: 'nowrap',
    boxSizing: 'border-box',
    ...(open && {
      ...openedMixin(theme),
      '& .MuiDrawer-paper': openedMixin(theme),
    }),
    ...(!open && {
      ...closedMixin(theme),
      '& .MuiDrawer-paper': closedMixin(theme),
    }),
  }),
);



export default function MiniDrawer(anchor) {
  const { auth } = useAuth()
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const openMenu = Boolean(anchorEl);
  const [selectedIndex, setSelectedIndex] = React.useState(1);
  const [expandBar, setexpandBar] = useState(false)

  let navigate = useNavigate();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    handleClose()
    localStorage.removeItem("user")
    navigate("/")
  }

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };
  const handleListItemClick = (event, index) => {
    event.preventDefault()
    setSelectedIndex(index);
  };

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />

      <AppBar position="fixed" open={open} sx={{ background: 'white' }}>
        <Toolbar sx={{ color: 'black' }}>
          <IconButton
            color="primary"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{
              marginRight: 5,
              ...(open && { display: 'none' }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" sx={{ color: 'black', fontFamily: 'Montserrat' }} noWrap component="div" align='right'>
            ADMIN DASHBOARD
          </Typography>

          <Typography
            variant="h6"
            noWrap
            component="div"
            align='right'
            sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' }, margin: 0.1, color: 'black' }}
          >
            {/* <ArrowDropDownIcon/>  */}
            <Avatar
              alt="Remy Sharp" src="/static/images/avatar/1.jpg"
              sx={{ m: '0 94%', border: '1px solid red' }}
            />
          </Typography>
          &nbsp;
          <Typography sx={{ display: { sm: 'none', md: 'block' }, fontFamily: 'Montserrat', fontWeight: 'bold' }}>
            {auth ? `${auth.user.firstname} ${auth.user.lastname}` : "Default"}
            <br />
            <Typography variant='subtitle1' style={{ fontSize: '10px', fontFamily: 'Montserrat' }} >administrateur</Typography>
          </Typography>
          {/* <IconButton 
                onClick={handleClick}
                aria-controls={open ? 'basic-menu' : undefined}
                aria-haspopup="true"
                aria-expanded={open ? 'true' : undefined}
            >
                <ArrowDropDownIcon/>
                <Menu
                    id="basic-menu"
                    anchorEl={anchorEl}
                    open={openMenu}
                    onClose={handleClose}
                    MenuListProps={{
                    'aria-labelledby': 'basic-button',
                    }}
                >
                    <MenuItem onClick={handleClose}>Profile</MenuItem>
                    <MenuItem onClick={handleClose}>My account</MenuItem>
                    <MenuItem onClick={handleClose}>Logout</MenuItem>
                </Menu>
            </IconButton>  */}

          <div>
            <IconButton
              id="basic-button"
              aria-controls={open ? 'basic-menu' : undefined}
              aria-haspopup="true"
              aria-expanded={open ? 'true' : undefined}
              onClick={handleClick}
            >
              <ArrowDropDownIcon />
            </IconButton>
            <Menu
              id="basic-menu"
              anchorEl={anchorEl}
              open={openMenu}
              onClose={handleClose}
              MenuListProps={{
                'aria-labelledby': 'basic-button',
              }}
            >
              <MenuItem onClick={handleClose}>Profile</MenuItem>
              <MenuItem onClick={handleClose}>My account</MenuItem>
              <MenuItem onClick={() => handleLogout()}>Logout</MenuItem>
            </Menu>
          </div>


        </Toolbar>
      </AppBar>



      <Drawer
        variant="permanent"
        open={open}
        PaperProps={{
          sx: {
            backgroundColor: "#093545",
            color: "#fff",
            fontFamily: 'Montserrat'
          }
        }}
      >
        <DrawerHeader>
          <Typography style={{ fontFamily: 'Montserrat', fontWeight: 'bold' }}>Money-Subscribe</Typography>
          <IconButton onClick={handleDrawerClose} sx={{ color: '#fff' }}>
            {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
          </IconButton>
        </DrawerHeader>
        <Divider />

        {/* {
          list('anchor')
        } */}

        

        {
          (auth && auth.user.roles) && auth.user.roles.includes(5150) ?(
            <Box
              sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 100, }}
              role="presentation"
              //   onClick={toggleDrawer(anchor, false)}
              //   onKeyDown={toggleDrawer(anchor, false)}
              mt={2}
            >

          <List>
            <ListItemButton
              selected={selectedIndex === 1}
              onClick={(event) => { handleListItemClick(event, 1); navigate('admin') }}
              sx={{ alignItems: 'start', minWidth: 240 }}
            >
              <ListItemIcon>
                <GridViewRoundedIcon sx={{ color: '#fff' }} />
              </ListItemIcon>
              Dashboard
              <ListItemText />
            </ListItemButton>
            {/* ))} */}
          </List>
          <Divider sx={{ minWidth: 238 }} />

          <Divider />
          <List style={{ marginBottom: 'auto' }}>
            <ListItemButton
              selected={selectedIndex === 2}
              onClick={(event) => { handleListItemClick(event, 2); navigate('admin/user') }}
              sx={{ alignItems: 'start', minWidth: 240 }}
            >
              <ListItemIcon>
                <PersonRoundedIcon sx={{ color: '#fff' }} />
              </ListItemIcon>
              Utilisateurs
              <ListItemText />
            </ListItemButton>
          </List>
          <Divider sx={{ minWidth: 238 }} />
          <List style={{ marginBottom: 'auto' }}>
            <ListItemButton
              selected={selectedIndex === 3}
              onClick={(event) => { handleListItemClick(event, 3); navigate('admin/cotisation') }}
              sx={{ alignItems: 'start', minWidth: 240 }}
            >
              <ListItemIcon>
                <MonetizationOnIcon sx={{ color: '#fff' }} />
              </ListItemIcon>
              Cotisations
              <ListItemText />
            </ListItemButton>
          </List>
          <Divider sx={{ minWidth: 238 }} />
          <List style={{ marginBottom: 'auto' }}>
            <ListItemButton
              selected={selectedIndex === 4}
              onClick={(event) => { handleListItemClick(event, 4); navigate('admin/profile') }}
              sx={{ alignItems: 'start', minWidth: 240 }}
            >
              <ListItemIcon>
                <SettingsIcon sx={{ color: '#fff' }} />
              </ListItemIcon>
                Parametres &nbsp;
                {
                  expandBar ? 
                              <ExpandLess onClick={()=>setexpandBar(false)}/>:
                              <ExpandMore onClick={()=>setexpandBar(true)}/>
                }

              <ListItemText />
              

            </ListItemButton>
          </List>


              {
              expandBar &&
                <Typography sx={{maxWidth:150, m:'0 70px' }}>
                  <ListItemButton
                    sx={{
                        minHeight: 48,
                        justifyContent: open ? 'initial' : 'center',
                        px: 2.5,
                        minWidth:163,
                        
                    }}
                  >
            
                    <ListItemIcon
                        sx={{
                        minWidth: 0,
                        mr: open ? -2 : 'auto',
                        justifyContent: 'center',
                        }}
                    >  
                    </ListItemIcon>

                      <ListItemText 
                      primary='Paramétres généraux' 
                      sx={{
                         opacity: open ? 1 : 0,
                         mr: open ? 3 : 'auto',
                         justifyContent: 'center', 
                      }} />
                    <Divider/>
                  </ListItemButton>

                  

                  <ListItemButton
                    sx={{
                        minHeight: 48,
                        justifyContent: open ? 'initial' : 'center',
                        px: 2.5,
                        minWidth:163,
                    }}
                  >
                
                    <ListItemIcon
                        sx={{
                        minWidth: 0,
                        mr: open ? -2 : 'auto',
                        justifyContent: 'center',
                        }}
                    >  
                    </ListItemIcon>

                      <ListItemText 
                          primary='Utilisateurs' 
                          sx={{
                            opacity: open ? 1 : 0,
                            mr: open ? 3 : 'auto',
                            justifyContent: 'center', 
                          }} 
                      />
                        
                        
                  </ListItemButton>
                  

                  <ListItemButton
                    sx={{
                        minHeight: 48,
                        justifyContent: open ? 'initial' : 'center',
                        px: 2.5,
                        minWidth:163,
                    }}
                  >
            
                    <ListItemIcon
                        sx={{
                        minWidth: 0,
                        mr: open ? -2 : 'auto',
                        justifyContent: 'center',
                        }}
                    >  
                    </ListItemIcon>

                    <ListItemText 
                      primary='Archives' 
                      sx={{
                        opacity: open ? 1 : 0,
                        mr: open ? 3 : 'auto',
                        justifyContent: 'center', 
                      }} 
                    />
                     
                      
                  </ListItemButton>

                  {/* <Divider sx={{ minWidth: 238 }} /> */}

                  <ListItemButton
                    sx={{
                        minHeight: 48,
                        justifyContent: open ? 'initial' : 'center',
                        px: 2.5,
                        minWidth:163
                    }}
                  >
            
                  <ListItemIcon
                      sx={{
                      minWidth: 0,
                      mr: open ? -2 : 'auto',
                      justifyContent: 'center',
                      }}
                  >  
                  </ListItemIcon>

                  <ListItemText 
                      primary='Membres bloqués' 
                      sx={{
                        opacity: open ? 1 : 0,
                        mr: open ? 3 : 'auto',
                        justifyContent: 'center', 
                      }} />
                     
                      
                  </ListItemButton>

                </Typography>
              }

            </Box>
            ):(
              <Box
              sx={{ width: anchor === 'top' || anchor === 'bottom' ? 'auto' : 100, }}
              role="presentation"
              //   onClick={toggleDrawer(anchor, false)}
              //   onKeyDown={toggleDrawer(anchor, false)}
              mt={2}
            >
    
              <List>
                <ListItemButton
                  selected={selectedIndex === 1}
                  onClick={(event) => { handleListItemClick(event, 1); navigate('admin') }}
                  sx={{ alignItems: 'start', minWidth: 240 }}
                >
                  <ListItemIcon>
                    <GridViewRoundedIcon sx={{ color: '#fff' }} />
                  </ListItemIcon>
                  Dashboard
                  <ListItemText />
                </ListItemButton>
                {/* ))} */}
              </List>
              <Divider sx={{ minWidth: 238 }} />
    
              <Divider />
              <List style={{ marginBottom: 'auto' }}>
                <ListItemButton
                  selected={selectedIndex === 2}
                  onClick={(event) => { handleListItemClick(event, 2); navigate('admin/user') }}
                  sx={{ alignItems: 'start', minWidth: 240 }}
                >
                  <ListItemIcon>
                    <PersonRoundedIcon sx={{ color: '#fff' }} />
                  </ListItemIcon>
                  Users
                  <ListItemText />
                </ListItemButton>
              </List>
              <Divider sx={{ minWidth: 238 }} />
              <List style={{ marginBottom: 'auto' }}>
                <ListItemButton
                  selected={selectedIndex === 3}
                  onClick={(event) => { handleListItemClick(event, 3); navigate('admin/cotisation') }}
                  sx={{ alignItems: 'start', minWidth: 240 }}
                >
                  <ListItemIcon>
                    <MonetizationOnIcon sx={{ color: '#fff' }} />
                  </ListItemIcon>
                  Cotisations
                  <ListItemText />
                </ListItemButton>
              </List>
              <Divider sx={{ minWidth: 238 }} />
              <List style={{ marginBottom: 'auto' }}>
                <ListItemButton
                  selected={selectedIndex === 4}
                  onClick={(event) => { handleListItemClick(event, 4); navigate('admin/profile') }}
                  sx={{ alignItems: 'start', minWidth: 240 }}
                >
                  <ListItemIcon>
                    <SettingsIcon sx={{ color: '#fff' }} />
                  </ListItemIcon>
                  Parametres 
                  <ListItemText />
                </ListItemButton>
              </List>
              </Box>
            )
        }

        <Divider />


      </Drawer>

    </Box>
  );
}
