import React, { PureComponent } from 'react';
import { PieChart, Pie, Cell, Legend } from 'recharts';

const data = [
  { name: 'Group A', value: 400 },
  { name: 'Group B', value: 300 },
  { name: 'Group C', value: 300 },
  { name: 'Group D', value: 200 },
];
const COLORS = ['#093545', '#20DF7F', '#FFBB28', '#FF8042'];



export default class ChartLineaire extends PureComponent {
 

  render() {
    return (
      <PieChart 
            width={250} 
            height={260} 
            onMouseEnter={this.onPieEnter} 
            style={{ margin: '0 auto',}}
        >
        <Pie
          data={data}
          cx={120}
          cy={100}
          innerRadius={60}
          outerRadius={80}
          fill="#8884d8"
          paddingAngle={0}
          dataKey="value"
        //   label={renderCustomizedLabel}
        >
          {data.map((entry, index) => (
            <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
          ))}
          <Legend verticalAlign="bottom" height={36}/>
        </Pie>
    
      </PieChart>
    );
  }
}
