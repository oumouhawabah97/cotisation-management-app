import * as React from 'react';
// import Box from '@mui/material/Box';
// import Button from '@mui/material/Button';
import { IconButton, Box } from '@mui/material';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export default function BasicModalC({ cotisation }) {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    return (
        <div>
            {/* <Button onClick={handleOpen}>Open modal</Button> */}
            <IconButton onClick={handleOpen}>
                <InfoOutlinedIcon />
            </IconButton>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        User Details
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        Montant: {cotisation && cotisation.amount}
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        Date: {cotisation && cotisation.date}
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        Seuil: {cotisation && cotisation.seuil}
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        status: {cotisation && (cotisation.published) ? "active" : "no-active"}
                    </Typography>
                </Box>
            </Modal>
        </div>
    );
}
