import React, { PureComponent } from 'react';
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Brush,
  ResponsiveContainer,
} from 'recharts';

const data = [
  {
    name: 'Janvier',
    uv: 0,
    pv: 2400,
    amt: 2400,
  },
  {
    name: 'Fevier',
    uv: 2500,
    pv: 1398,
    amt: 2210,
  },
  {
    name: 'Mars',
    uv: 1800,
    pv: 9800,
    amt: 2290,
  },
  {
    name: 'Avriel',
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: 'Mai',
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: 'Juin',
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
  {
    name: 'Juillet',
    uv: 3490,
    pv: 4300,
    amt: 2100,
    // mt:60,
  },
];

export default class ProgresLineaire extends PureComponent {
 
  render() {
    return (
      <div style={{ minWidth: '120%' }}>
       

        <ResponsiveContainer width="80%" height={250}>
          <LineChart
            width={400}
            height={400}
            data={data}
            syncId="anyId"
            margin={{
              top: 10,
              right: 30,
              left: 0,
              bottom: 0,
            }}
          >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Line type="monotone" dataKey="uv" stroke="#8884d8" fill="#8884d8" />
          </LineChart>
        </ResponsiveContainer>
      

       
       
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="name" />
            <YAxis />
            <Tooltip />
            <Line type="monotone" dataKey="pv" stroke="#82ca9d" fill="#82ca9d" />
            <Brush />
      
      </div>
    );
  }
}
