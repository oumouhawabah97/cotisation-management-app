import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import Progess from './Progess'
import ProgresLineaire from './ProgresLineaire'

export default function Parametre() {
  return (
    <div className='container-fluid'>
   <div className='row'>
    <div className='col-2'>

    </div>
    <div className='col'>
    <div class="row row-cols-1 row-cols-md-3 mt-5">
  <div class="col mt-5">
    <div class="card h-80 ">
   
      <div class="card-body">
      
     <p>Aujourd'hui</p>
     <h3>15.000CFA</h3>
     <p>Nombre de cotisation:27</p>
      </div>
    
    </div>
  </div>
  <div class="col mt-5">
    <div class="card h-80">
     
      <div class="card-body">
      <p>Aujourd'hui</p>
     <h3>15.000CFA</h3>
     <p>Nombre de cotisation:27</p>
      </div>
    
    </div>
  </div>
  <div class="col mt-5">
    <div class="card h-80">
    
      <div class="card-body">
      <p>Aujourd'hui</p>
     <h3>15.000CFA</h3>
     <progress value="100" max="100"  >100 %</progress><br />
      </div>
      
    </div>
  </div>
</div>

<div class="row">
  <div class="col-sm-7">
    <div class="card mt-4 ">
    <div className="card-header" style={{background:'#20DF7F'}}>
   Evolution des cotisation en fonction du temps
  </div>
  <br/>
      <div className="card-body">
      <ProgresLineaire/>
      </div>
    </div>
  </div>
  <div class="col-sm-5">
    <div class="card mt-4">
    <div class="card-header"  style={{background:'#20DF7F'}}>
   Statistique
  </div>
      <div class="card-body">
      <Progess/>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-sm-7">
    <div class="card mt-5">
  
      <div class="card-body m-0 p-0">
      <table class="table"  >
  <thead>
    <tr  style={{background:'#20DF7F'}}>
    <th scope="col">Membre</th>
      <th scope="col">Horaire</th>
      <th scope="col">Montant</th>
      <th scope="col">Statut</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td  style={{fontSize:14}}>Selena Roy</td>
      <td>11h30</td>
      <td>1.000 FCFA</td>
      <td><button type="button" class="btn btn-success"style={{fontSize:12}}>Valide</button></td>
    </tr>
    <tr>
      <td>Jhon Robert</td>
      <td>12h30</td>
    
      <td>2.000 FCFA</td>
      <td><button type="button" class="btn btn-danger " style={{fontSize:12}}> retard</button></td>
   
    </tr>
    <tr>
      <td > Anne Hathaway</td>
      <td>8h00</td>
      <td>2.000 FCFA</td>
      <td><button type="button" class="btn btn-success" style={{fontSize:12}}>Valide</button></td>
    </tr>
    <tr>
      <td>Ravi Shankar</td>
      <td>22h25</td>
      <td>1.000 FCFA</td>
      <td><button type="button" class="btn btn-danger " style={{fontSize:12}}> retard</button></td>
    </tr>

    <tr>
      <td > Ravi Shankar</td>
      <td></td>
      <td></td>
      <td><button type="button" class="btn btn-secondary" style={{fontSize:11}}>Absent</button></td>
    </tr>

    <tr>
    <td>  Emma Stone</td>
      <td>14h15</td>
      <td>5.000 FCFA</td>
      <td><button type="button" class="btn btn-success" style={{fontSize:12}}>Valide</button></td>
    </tr>
  </tbody>
      </table>
      </div>
    </div>
  </div>
  <div class="col-sm-5">
    <div class="card  mt-5">
  
      <div class="card-body m-0 p-0">
      <table class="table">
  <thead>
    <tr  style={{background:'#20DF7F'}}>
      <th scope="col">Membre</th>
      <th scope="col">Progression</th>
      <th scope="col">Date</th>
    
    </tr>
  </thead>
  <tbody>
    <tr>
    <td  style={{fontSize:14}} >Selena Roy</td>
      <td>
      
      <progress value="100" max="100" className='success
      ' ></progress>100 %
      </td>
      <td style={{fontSize:14}}>12-05-2022</td>
     
    </tr>
    <tr>
    <td>Jhon Robert</td>
    <td>
      
      <progress value="100" max="100"  >100 %</progress><br />
      </td>
      <td>12-05-2022</td>
     
    </tr>
    <tr>
      <td> Anne Hathaway</td>
      <td>
      
      <progress value="60" max="100"  >100 %</progress><br />
      </td>
      <td>12-05-2022</td>
     
    </tr>
    
    <tr>
      <td > Ravi Shankar</td>
      <td>
      
      <progress value="50" max="100"  >100 %</progress><br />
      </td>
      <td> <td>12-05-2022</td></td>
    
    </tr>

    <tr>
      <td> Emma Stone</td>
      <td>
      
      <progress value="40" max="100"  >100 %</progress><br />
      </td>
      <td>12-05-2022</td>
    
    </tr>
    <tr>
    <td >Selena Roy</td>
      <td>
      
      <progress value="40" max="100"  >100 %</progress><br />
      </td>
      <td>12-05-2022</td>
     
    </tr>
   
  </tbody>
      </table>
      </div>
    </div>
  </div>
</div>
{/* <nav aria-label="..."> */}
  <ul class="pagination mt-2">
    <li class="page-item disabled m-1 br-5">
      <span class="page-link">Previous page</span>
    </li>
    <li className="page-item m-1 br-15" style={{borderRaduis:5}}><a class="page-link" href="/">1</a></li>
    <li class="page-item  m-1 br-15" aria-current="page">
      <span class="page-link">2</span>
    </li>
    <li class="page-item  m-1 br-15"><a class="page-link" href="/">3</a></li>
    <li class="page-item  m-1 br-15"><a class="page-link" href="/">4</a></li>
    <li class="page-item  m-1 br-15">
      <a class="page-link" href="/">Next page</a>
    </li>
  </ul>
{/* </nav> */}
   
    </div>

   </div>


    </div>
  )
}

