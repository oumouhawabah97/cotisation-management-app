import * as React from 'react';
// import Box from '@mui/material/Box';
// import Button from '@mui/material/Button';
import { IconButton, Box } from '@mui/material';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export default function BasicModal({user}) {
    const [open, setOpen] = React.useState(false);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);

    return (
        <div>
            {/* <Button onClick={handleOpen}>Open modal</Button> */}
            <IconButton onClick={handleOpen}>
                <InfoOutlinedIcon />
            </IconButton>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography id="modal-modal-title" variant="h6" component="h2">
                        User Details
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        Nom: {user&& user.firstname} {user&&user.lastname}
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        Email: {user&& user.email}
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        Telephone: {user&& user.phone}
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        Organisation: {user&& user.organisation}
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        Profession: {user&& user.profession}
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        Adresse: {user&& user.address}
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        Anniversaire: {user&& user.birthday}
                    </Typography>
                    <Typography id="modal-modal-description" sx={{ mt: 2 }}>
                        status: {user&& (user.archiver||user.blocked) ?"no active" : "active"}
                    </Typography>
                </Box>
            </Modal>
        </div>
    );
}
