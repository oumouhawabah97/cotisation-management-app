import * as React from 'react';
// import Table from '@mui/material/Table';
// import TableBody from '@mui/material/TableBody';
// import TableCell from '@mui/material/TableCell';
// import TableContainer from '@mui/material/TableContainer';
// import TableHead from '@mui/material/TableHead';
// import TableRow from '@mui/material/TableRow';
// import Paper from '@mui/material/Paper';
// import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
// import ArchiveOutlinedIcon from '@mui/icons-material/ArchiveOutlined';
// import LockOpenOutlinedIcon from '@mui/icons-material/LockOpenOutlined';
import EditIcon from '@mui/icons-material/Edit';
import client from "../../services/clientService"
// import BasicModal from "../UserInfoModal"
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import {  Chip, Dialog, DialogContent, DialogContentText, DialogTitle, Divider, IconButton, List, ListItem, ListItemText, Typography } from '@mui/material';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import { ToastContainer, toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';
import moment from 'moment';

function tokenHeader() {
    const user = JSON.parse(localStorage.getItem("user"))

    if (user && user.accessToken) {
        return { "Authorization": "Bearer " + user.accessToken }
    }
}





 


// toast.success("Success Notification !", {
//   position: toast.POSITION.TOP_CENTER
// });

// toast.error("Error Notification !", {
//   position: toast.POSITION.TOP_LEFT
// });

// toast.warn("Warning Notification !", {
//   position: toast.POSITION.BOTTOM_LEFT
// });

// toast.info("Info Notification !", {
//   position: toast.POSITION.BOTTOM_CENTER
// });

// toast("Custom Style Notification with css class!", {
//   position: toast.POSITION.BOTTOM_RIGHT,
//   className: 'foo-bar'
// });

const notify=() => {
  toast.warning("Utilisateur déjà bloqué !", {
    position: toast.POSITION.BOTTOM_CENTER
  })
  return false
}

const handleBlock = async (id) => {
  const response = await client.put(`/users/edit/${id}`, { blocked: true }, { headers: tokenHeader() })
  const { data } = await response;
  console.log(data);
  if(data.result){
    data.result.archiver?(
      notify()
    ):(
      toast.info("Bloqué avec succés !", {
        position: toast.POSITION.BOTTOM_CENTER
      })
    ) 
    setTimeout(()=>{
      window.location.reload()
    },2000)
  }
}

// const handleArchiver = async (id) => {
//   const response = await client.put(`/users/edit/${id}`, { archiver: true }, { headers: tokenHeader() })
//   const { data } = await response;
  
//   if(data.result){
//     data.result.archiver?(
//       toast.warning("Utilisateur déjà archivé !", {
//         position: toast.POSITION.BOTTOM_CENTER
//       })
//     ):(
//       toast.info("Archivé avec succés !", {
//         position: toast.POSITION.BOTTOM_CENTER
//       })
//     ) 
//     setTimeout(()=>{
//       window.location.reload()
//     },2000)
//   }
// }

function AlertDialog({userDetails, time}) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  console.log(userDetails)

  return (
    <div>
    
      <IconButton onClick={handleClickOpen}>
            <InfoOutlinedIcon fontSize='small' sx={{color:'#093545'}}/>
        </IconButton>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        
      >
        <DialogTitle id="alert-dialog-title">
          {"Détails cotisation de l'utilisateur"}
        </DialogTitle>
        <DialogContent sx={{maxWidth:'100vh'}}>
          <DialogContentText id="alert-dialog-description">
          <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
            {/* {[1,2,3].map((value) => ( */}
              <ListItem
                key={userDetails._id}
                disableGutters
                secondaryAction={                    
                   <Chip  label={moment(userDetails.date).format('DD-MM-YYYY')}/>
                }
              >
                <ListItemText style={{fontWeight:'bold'}} primary={`Date`} />
              </ListItem>
              <Divider />
              <ListItem
                key={userDetails._id}
                disableGutters
                secondaryAction={ 
                   <Chip label={moment(time.createdAt).format('HH:MM:SS')}/>
                }
              >
                <ListItemText primary={`Heure`} />
              </ListItem>
              <Divider />
              <ListItem
                key={userDetails._id}
                disableGutters
                secondaryAction={ 
                   <Chip  label={userDetails.amount}/>
                }
              >
                <ListItemText primary={`Montant`} />
              </ListItem>
              <Divider />
              <ListItem
                key={userDetails._id}
                disableGutters
                // secondaryAction={ 
                //    <Chip  label={userDetails.published}/>
                // }
                secondaryAction={ 
                    userDetails.published? <Chip color='success' label='Validé'/>:<Chip color='error' label='Invalide'/>  
                 }
              >
                <ListItemText primary={`Status`} />
              </ListItem>
              <Divider />
              
            {/* ))} */}
          </List>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  );
}

export default function TableCotiseAdmin({users}) {
  const rowsUser = users;
    

  const columns = [
      { 
        field: 'Mois',
        headerName: 'Membres', 
        width: 150,
        renderCell: (params) => (
            <>     
                {
                    // console.log(params.row.user)
                    <div style={{display: 'flex', fontFamily:'Montserrat'}}>
          
                        <Typography >{params.row.user.firstname}</Typography> &nbsp;
                        <Typography >{params.row.user.lastname}</Typography>
                    </div>
                }
            </>
        ) 
      },
      {
        field: 'date',
        headerName: 'Date début',
        width: 160,
        renderCell: (params) => (    
            <Typography variant='body2' component='span' sx={{color:'#093545', fontFamily:'Montserrat'}}>
                {moment(params.row.date).format('DD-MM-YYYY')}
            </Typography>
        )
      },
      {
        field: 'amount',
        headerName: 'Motant cotisé',
        width: 160,
      },
      {
       
        field: 'published',
        headerName: 'Status',
        width: 130,
        renderCell: (params) => (
            <>            
                {
                  params.row.published ?(
                    <Typography variant='body2' color='success' component='span'>termine</Typography>
      
                  ):(
                    <Typography variant='body2' component='span' sx={{color:'#093545', fontFamily:'Montserrat'}}>En attente</Typography>
                  )
                }
              </>
        )
      },
      {
        // field: 'status',
        headerName: 'Action',
        description: 'This column has a value getter and is not sortable.',
        sortable: false,
        width: 140,
        renderCell:(params)=>(
            <>
              <Box sx={{
                width: '100%',
                    display:'flex'
                }}
                >
                <AlertDialog userDetails={params.row} time={params.row.user}/> 
                <IconButton onClick={()=>handleBlock(params.row._id && params.row._id)}>
                    <EditIcon fontSize='small' sx={{color:'#093545'}}/>
                </IconButton>
              
            </Box>
            </>
          ) 
      },
    ];

  return (
    <>
      <DataGrid
        icons
        getRowId={(rowsUser) => rowsUser._id}
        rows={rowsUser}
        columns={columns}
        pageSize={4}
        rowsPerPageOptions={[4]}
        disableSelectionOnClick
        sx={{
            height:350,
            "& .MuiDataGrid-columnHeaders": {
            backgroundColor: "#20df7f",
            color: "#000",
            fontSize: 16,
            justifyContent: 'left'
          },
          // "&.MuiDataGrid-cellCenter": {
          //   justifyContent: 'center',
          //   m:'0 auto'
          // },
          "&.MuiDataGrid-cellContent":{
            justifyContent: 'center',
            m:'0 auto',
            border:'none'
          },
          fontFamily:'Montserrat',
          "&.MuiDataGrid-row":{
            justifyContent: 'center',
            m:'0 auto',
            border:'none'
          }
        }}
      />
      <ToastContainer />
    </>
  );
}

