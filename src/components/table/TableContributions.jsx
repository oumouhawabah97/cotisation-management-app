import * as React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
// import { IconButton } from '@mui/material';
// import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
// import ArchiveOutlinedIcon from '@mui/icons-material/ArchiveOutlined';
// import LockOpenOutlinedIcon from '@mui/icons-material/LockOpenOutlined';
// import client from "../../services/clientService"
import BasicModalC from "../CotisationInfoModal"

// function tokenHeader() {
//     const user = JSON.parse(localStorage.getItem("user"))

//     if (user && user.accessToken) {
//         return { "Authorization": "Bearer " + user.accessToken }
//     }
// }

export default function BasicTable({ cotisations }) {
console.log(cotisations)
    // const handleActive = async (id) => {
    //     const response = await client.put(`/users/edit/${id}`, { blocked:false}, { headers: tokenHeader() })
    //     const { data } = await response;
    //     console.log(data);
    // }
    // const handleBlock = async (id) => {
    //     const response = await client.put(`/users/edit/${id}`, { blocked: true }, { headers: tokenHeader() })
    //     const { data } = await response;
    //     console.log(data);
    // }
    // const handleArchiver = async (id) => {
    //     const response = await client.put(`/users/edit/${id}`, { archiver: true }, { headers: tokenHeader() })
    //     const { data } = await response;
    //     console.log(data);
    // }
    return (
        <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Montant Cotise</TableCell>
                        <TableCell align="left">Date</TableCell>
                        <TableCell align="left">Montant Restant</TableCell>
                        <TableCell align="right">Actions</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {cotisations && cotisations.map((cotisation) => (
                        <TableRow
                            key={cotisation._id}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                        >
                            <TableCell component="th" scope="row">
                                {cotisation.amount}
                            </TableCell>
                            <TableCell align="left">{cotisation.date}</TableCell>
                            <TableCell align="left">{cotisation && cotisation.seuil - cotisation.amount}</TableCell>
                            <TableCell align="right">


                                <div style={{ display: "flex", justifyContent: "flex-end" }}>
                                    <BasicModalC cotisation={cotisation} />
                                    {/* <IconButton onClick={() => handleArchiver(cotisation._id)}>

                                        <ArchiveOutlinedIcon />
                                    </IconButton> */}
                                    {/* <IconButton onClick={() => handleBlock(cotisation._id)}>
                                        <LockOpenOutlinedIcon />
                                    </IconButton> */}
                                </div>
                                {/* <InfoOutlinedIcon /> */}

                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
