import * as React from 'react';
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import { Chip, Dialog, DialogContent, DialogContentText, DialogTitle, Divider, IconButton, LinearProgress, List, ListItem, ListItemText, Typography } from '@mui/material';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import ArchiveOutlinedIcon from '@mui/icons-material/ArchiveOutlined';
import LockOpenOutlinedIcon from '@mui/icons-material/LockOpenOutlined';
import client from '../../services/clientService';
import { ToastContainer, toast } from 'react-toastify';

import 'react-toastify/dist/ReactToastify.css';
import { BlockOutlined } from '@mui/icons-material';
import moment from 'moment';



  


function tokenHeader() {
    const user = JSON.parse(localStorage.getItem("user"))

    if (user && user.accessToken) {
        return { "Authorization": "Bearer " + user.accessToken }
    }
}
// toast.success("Success Notification !", {
//   position: toast.POSITION.TOP_CENTER
// });

// toast.error("Error Notification !", {
//   position: toast.POSITION.TOP_LEFT
// });

// toast.warn("Warning Notification !", {
//   position: toast.POSITION.BOTTOM_LEFT
// });

// toast.info("Info Notification !", {
//   position: toast.POSITION.BOTTOM_CENTER
// });

// toast("Custom Style Notification with css class!", {
//   position: toast.POSITION.BOTTOM_RIGHT,
//   className: 'foo-bar'
// });

const notify=() => {
  toast.warning("Utilisateur déjà bloqué !", {
    position: toast.POSITION.BOTTOM_CENTER
  })
  return false
}

const handleBlock = async (id) => {
  const response = await client.put(`/users/edit/${id}`, { blocked: true }, { headers: tokenHeader() })
  const { data } = await response;
 
  if(data.result){
    data.result.archiver?(
      notify()
    ):(
      toast.info("Bloqué avec succés !", {
        position: toast.POSITION.BOTTOM_CENTER
      })
    ) 
    setTimeout(()=>{
      window.location.reload()
    },2000)
  }
}

const handleArchiver = async (id) => {
  const response = await client.put(`/users/edit/${id}`, { archiver: true }, { headers: tokenHeader() })
  const { data } = await response;
  if(data.result){
    data.result.archiver?(
      toast.warning("Utilisateur déjà archivé !", {
        position: toast.POSITION.BOTTOM_CENTER
      })
    ):(
      toast.info("Archivé avec succés !", {
        position: toast.POSITION.BOTTOM_CENTER
      })
    ) 
    setTimeout(()=>{
      window.location.reload()
    },2000)
  }
}

function AlertDialog({userDetails}) {
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };


  return (
    <div>
    
      <IconButton onClick={handleClickOpen} title='detail'>
            <InfoOutlinedIcon fontSize='small' sx={{color:'#093545'}}/>
        </IconButton>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        
      >
        <DialogTitle id="alert-dialog-title">
          {"Détails Concernant L'utilisateur"}
        </DialogTitle>
        <DialogContent sx={{maxWidth:'100vh'}}>
          <DialogContentText id="alert-dialog-description">
          <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}>
            {/* {[1,2,3].map((value) => ( */}
              <ListItem
                key={userDetails._id}
                disableGutters
                secondaryAction={                    
                   <Chip  label={userDetails.firstname}/>
                }
              >
                <ListItemText style={{fontWeight:'bold'}} primary={`Prénom`} />
              </ListItem>
              <Divider />
              <ListItem
                key={userDetails._id}
                disableGutters
                secondaryAction={ 
                   <Chip label={userDetails.lastname}/>
                }
              >
                <ListItemText primary={`Nom`} />
              </ListItem>
              <Divider />
              <ListItem
                key={userDetails._id}
                disableGutters
                secondaryAction={ 
                   <Chip  label={userDetails.address}/>
                }
              >
                <ListItemText primary={`Adresse`} />
              </ListItem>
              <Divider />
              <ListItem
                key={userDetails._id}
                disableGutters
                secondaryAction={ 
                   <Chip  label={userDetails.email}/>
                }
              >
                <ListItemText primary={`Email`} />
              </ListItem>
              <Divider />
              <ListItem
                key={userDetails._id}
                disableGutters
                secondaryAction={ 
                   <Chip  label={userDetails.profession}/>  
                }
              >
                <ListItemText primary={`Profession`} />
              </ListItem>
              <Divider />
              <ListItem
                key={userDetails._id}
                disableGutters
                secondaryAction={ 
                   !userDetails.blocked? <Chip color='success' label='Actif'/>:<Chip color='error' label='Inactif'/>  
                }
              >
                <ListItemText primary={`Status`} />
              </ListItem>
              <Divider />
            {/* ))} */}
          </List>
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  );
}


export default function DataGridDemo({users}) {
  const rowsUser = users;
  // console.log(rowsUser && rowsUser.filter(x=> x.id === x.user.id))
  // const [cotisations, setCotisations] = React.useState('')

  const getCotisations = async () => {
    // const response = await client.get("/contributions/all/list", { headers: tokenHeader() })
    // const { data } = await response
    // setCotisations(data)
  }

  React.useEffect(() => {
    getCotisations()
  }, [])


  

  
  const columns = [
    {  headerName: 'Membres', width: 140,
      renderCell: (params) => (
        <div style={{display: 'flex', fontFamily:'Montserrat'}}>
          
          <Typography >{params.row.user.firstname}</Typography> &nbsp;
          <Typography >{params.row.user.lastname}</Typography>
        </div>
      )  
  },
    { field: 'birthday', headerName: 'Date debut', width: 130,
      renderCell: (params) => (
        <div style={{display: 'flex', fontFamily:'Montserrat'}}>
          
          <Typography >{moment(params.row.createdAt).format('DD-MM-YYYY')}</Typography> &nbsp;
        </div>
      )  
    },
    { field: 'Seuil', headerName: 'Seuil', width: 120, 
      renderCell: (params) => (
        <div style={{display: 'flex', fontFamily:'Montserrat'}}>
          
          <Typography >{params.row.seuil}</Typography> &nbsp;
          {/* <Typography >{params.row.lastname}</Typography> */}
        </div>
      )  
    },
    { field: 'progression', headerName: 'Progression', width: 150,
        renderCell: (params) => (
          <div style={{display: 'flex', fontFamily:'Montserrat',minWidth: '100%'}}>
            
            {
              Math.round((users && users.filter(x=>x.user._id === params.row.user._id)
              .reduce((acc,curr)=> acc+curr.amount,0) * 100) / 300000) === 100?(
                  <>
                    <Typography sx={{minWidth: '90%',display: 'flex', alignItems:'center'}}>
              
                      <Box sx={{ width: '80%' }}>
                        <LinearProgress 
                          variant="determinate" 
                          value={
                            (users && users.filter(x=>x.user._id === params.row.user._id)
                            .reduce((acc,curr)=> acc+curr.amount,0) * 100) / 300000
                          } 
                          sx={{ 
                            background: '#20df7f', 
                            // color:'#20df7f',
                            '&.MuiLinearProgress-root':{
                              color:'#20df7f'
                            },
                          }} 
                          color='success'
                          />
                      </Box>
                      &nbsp;
                      <Typography variant='body2'>
                          {
                            Math.round((users && users.filter(x=>x.user._id === params.row.user._id)
                            .reduce((acc,curr)=> acc+curr.amount,0) * 100) / 300000)
                          }%
                      </Typography>
                    </Typography> &nbsp;
                  </>
              ):(
                <>
                  <Typography sx={{minWidth: '90%',display: 'flex', alignItems:'center'}}>
              
                    <Box sx={{ width: '80%' }}>
                      <LinearProgress 
                        variant="determinate" 
                        value={
                          (users && users.filter(x=>x.user._id === params.row.user._id)
                          .reduce((acc,curr)=> acc+curr.amount,0) * 100) / 300000
                        } 
                        sx={{ background: '#20df7f',color:'blueviolet' }} 
                        />
                    </Box>
                    &nbsp;
                    {
                      Math.round((users && users.filter(x=>x.user._id === params.row.user._id)
                      .reduce((acc,curr)=> acc+curr.amount,0) * 100) / 300000)
                    }%
                  </Typography> &nbsp;
                </>
              )
            }
            
          </div>
        )  
    },
    {
      field: 'verified', headerName: 'Status', width: 90,
  
      renderCell: (params) => (
        <>
            <Box sx={{
              width: '100%'
            }}
          >
            {
                
              params.row.verified && <Typography variant='body2' color='success' component='span'>termine</Typography>
              //  ?(
              //   <Typography variant='body2' color='success' component='span'>termine</Typography>
  
              // ):(
              //   <Typography variant='body2' component='span' sx={{color:'#093545', fontFamily:'Montserrat'}}>En cours</Typography>
              // )
            }
            {
              (!params.row.verified && Math.round((users && users.filter(x=>x.user._id === params.row.user._id)
              .reduce((acc,curr)=> acc+curr.amount,0) * 100) / 300000) !== 100) && <Typography variant='body2' color='success' component='span'>En cours</Typography>
            }
            {
              Math.round((users && users.filter(x=>x.user._id === params.row.user._id)
              .reduce((acc,curr)=> acc+curr.amount,0) * 100) / 300000) === 100 && <Typography variant='body2' color='success' component='span'>termine</Typography>
  
            }
            {/* {
              cotisations && cotisations.map((data,index)=>{
                if(data.id === params.row.user){
                  console.log(data)
                }
              })
            } */}
          </Box>
          </>
      ),
  
    },
    { 
      field: 'action', headerName: 'Actions', width: 120,
      renderCell:(params)=>(
        <>
          <Box sx={{
            width: '100%',
            display:'flex'
          }}
        >
          <AlertDialog userDetails={params.row.user}/> 
          {
            params.row.archiver ?(
              <IconButton disabled>   
                  <ArchiveOutlinedIcon fontSize='small' sx={{color:'#f58'}}/>
              </IconButton>
            ):(
              <IconButton onClick={()=>handleArchiver(params.row.user._id && params.row.user._id)} title='archiver'>   
                <ArchiveOutlinedIcon fontSize='small' sx={{color:'#093545'}}/>
              </IconButton>
            )
          }
          {
            params.row.blocked ?(
              <IconButton disabled >
                  <BlockOutlined color='error' fontSize='small' />
              </IconButton>
            ):(
              <IconButton onClick={()=>handleBlock(params.row.user._id && params.row.user._id)} title='bloquer'>
                <LockOpenOutlinedIcon fontSize='small' sx={{color:'#093545'}}/>
              </IconButton>
            )
          }
          
          
        </Box>
        </>
      ) 
    }
  ];


  return (
    <>
      <DataGrid
        icons
        getRowId={(rowsUser) => rowsUser._id}
        rows={rowsUser}
        columns={columns}
        pageSize={4}
        rowsPerPageOptions={[5]}
        // checkboxSelection
        disableSelectionOnClick
        sx={{
            height:350,
            "& .MuiDataGrid-columnHeaders": {
            backgroundColor: "#20df7f",
            color: "#000",
            fontSize: 16
          },
          // "&.MuiDataGrid-cellCenter": {
          //   justifyContent: 'center',
          //   m:'0 auto'
          // },
          "&.MuiDataGrid-cellContent":{
            justifyContent: 'center',
            m:'0 auto'
          },
          fontFamily:'Montserrat'
        }}
      />
      <ToastContainer />
    </>
  );
}
